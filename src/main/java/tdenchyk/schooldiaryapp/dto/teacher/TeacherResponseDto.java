package tdenchyk.schooldiaryapp.dto.teacher;

public record TeacherResponseDto(Long id,
                                 String firstName,
                                 String lastName,
                                 String phoneNumber,
                                 String subjectId) {
}
