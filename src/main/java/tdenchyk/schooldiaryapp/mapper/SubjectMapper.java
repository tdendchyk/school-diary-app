package tdenchyk.schooldiaryapp.mapper;

import org.mapstruct.Mapper;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.subject.SubjectRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.model.Subject;

@Mapper(config = MapperConfig.class)
public interface SubjectMapper {
    SubjectResponseDto toDto(final Subject subject);

    Subject toSubject(final SubjectRequestDto requestDto);
}
