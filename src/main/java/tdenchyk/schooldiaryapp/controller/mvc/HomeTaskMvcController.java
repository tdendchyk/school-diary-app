package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleRequestDto;
import tdenchyk.schooldiaryapp.service.HomeTaskService;
import tdenchyk.schooldiaryapp.service.SubjectService;

@Controller
@RequiredArgsConstructor
@Tag(name = "MVC Hometask Controller", description = "Add new hometask")
@RequestMapping("/hometask")
public class HomeTaskMvcController {
    private final HomeTaskService homeTaskService;
    private final SubjectService subjectService;

    @Transactional
    @Operation(summary = "Add a new hometask for a certain subject")
    @PostMapping("/add")
    public String addNewHomeTask(
            @ModelAttribute("hometaskRequest") @Valid final HomeTaskRequestDto requestDto,
            @ModelAttribute("scheduleRequest") ScheduleRequestDto scheduleRequest,
            Model model, BindingResult result) {
        model.addAttribute("subjects", subjectService.findAllSubjects());
        if (result.hasErrors()) {
            return "schedule/teacher-schedule";
        }
        homeTaskService.addHomeTask(requestDto);
        return "hometask/hometask-successfully-added";
    }

    @Operation(summary = "Delete an existent hometask from DB")
    @PostMapping("/delete/all")
    public String deleteHomeTask(@RequestParam("taskIds") List<Long> taskIds) {
        for (Long id : taskIds) {
            homeTaskService.deleteById(id);
        }
        return "hometask/hometask-successfully-deleted";
    }
}
