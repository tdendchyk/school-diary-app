package tdenchyk.schooldiaryapp.dto.schedule;

import java.time.LocalTime;
import java.util.List;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.model.Schedule;

public record GetScheduleResponseDto(Long id,
                                     Long classId,
                                     Schedule.Day day,
                                     Long subjectId,
                                     List<HomeTaskResponseDto> tasks,
                                     LocalTime startTime,
                                     LocalTime endTime) {
    public GetScheduleResponseDto withTasks(List<HomeTaskResponseDto> tasks) {
        return new GetScheduleResponseDto(id, classId, day,
                subjectId, tasks, startTime, endTime);
    }
}
