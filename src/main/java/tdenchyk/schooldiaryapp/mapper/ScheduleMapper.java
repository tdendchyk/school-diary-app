package tdenchyk.schooldiaryapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.schedule.CreateScheduleDto;
import tdenchyk.schooldiaryapp.dto.schedule.GetScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleResponseDto;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.service.ClassService;
import tdenchyk.schooldiaryapp.service.SubjectService;

@Mapper(config = MapperConfig.class, uses = {SubjectService.class, ClassService.class})
public interface ScheduleMapper {
    @Mapping(source = "classId", target = "schoolClass")
    @Mapping(source = "subjectId", target = "subject")
    @Mapping(target = "homeTasks", ignore = true)
    Schedule toEntity(CreateScheduleDto scheduleDto);

    @Mapping(source = "schoolClass.id", target = "classId")
    @Mapping(source = "subject.id", target = "subjectId")
    ScheduleResponseDto toDto(Schedule schedule);

    @Mapping(source = "schoolClass.id", target = "classId")
    @Mapping(source = "subject.id", target = "subjectId")
    @Mapping(target = "tasks", ignore = true)
    GetScheduleResponseDto toResponseDto(Schedule schedule);

}
