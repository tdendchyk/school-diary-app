package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.CreateScheduleDto;
import tdenchyk.schooldiaryapp.dto.schedule.GetScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.TeacherScheduleRequestDto;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.HomeTaskMapper;
import tdenchyk.schooldiaryapp.mapper.ScheduleMapper;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.HomeTask;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.repository.ClassRepository;
import tdenchyk.schooldiaryapp.repository.ScheduleRepository;
import tdenchyk.schooldiaryapp.service.impl.ScheduleServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ScheduleServiceTest {
    @Mock
    private ScheduleMapper scheduleMapper;
    @Mock
    private HomeTaskMapper homeTaskMapper;
    @Mock
    private ScheduleRepository scheduleRepository;
    @Mock
    private ClassRepository classRepository;
    @InjectMocks
    private ScheduleServiceImpl scheduleService;

    @Test
    @DisplayName("Find schedule by id")
    public void findById_ValidId_ReturnsSchedule() {
        Schedule schedule = new Schedule();
        schedule.setId(1L);

        when(scheduleRepository.findById(any(Long.class))).thenReturn(Optional.of(schedule));

        Schedule actual = scheduleService.findById(1L);

        assertThat(actual.getId()).isEqualTo(1L);
    }

    @Test
    @DisplayName("Fail on finding schedule by invalid id")
    public void findById_InvalidId_ThrowsEntityNotFoundException() {
        when(scheduleRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> scheduleService.findById(1L));
    }

    @Test
    @DisplayName("Get teacher's schedule")
    public void getTeachersSchedule_ReturnsListOfGetScheduleResponseDto() {
        HomeTask homeTask1 = new HomeTask();
        homeTask1.setTaskDeadline(LocalDateTime.of(2024, 2, 12, 10, 30));
        HomeTask homeTask2 = new HomeTask();
        homeTask2.setTaskDeadline(LocalDateTime.of(2024, 2, 13, 11, 30));
        List<HomeTask> homeTasks = new ArrayList<>();
        homeTasks.add(homeTask1);
        homeTasks.add(homeTask2);
        Schedule schedule1 = new Schedule();
        schedule1.setId(1L);
        schedule1.setDay(Schedule.Day.MONDAY);
        schedule1.setHomeTasks(homeTasks);
        Schedule schedule2 = new Schedule();
        schedule2.setId(2L);
        schedule2.setDay(Schedule.Day.TUESDAY);

        TeacherScheduleRequestDto requestDto = new TeacherScheduleRequestDto(
                1L, Schedule.Day.MONDAY,
                LocalDate.of(2024, 2, 12));
        HomeTaskResponseDto homeTaskResponseDto1 = new HomeTaskResponseDto(
                1L, "Read 1st text",
                LocalDateTime.of(2024, 2, 12, 10, 30),
                1L);
        GetScheduleResponseDto responseDto1 = new GetScheduleResponseDto(1L,
                1L, Schedule.Day.MONDAY, 1L,
                List.of(homeTaskResponseDto1),
                LocalTime.of(10, 30), LocalTime.of(11, 15));

        when(scheduleRepository.findSchedulesBySubjectId(any(Long.class)))
                .thenReturn(List.of(schedule1, schedule2));
        when(scheduleMapper.toResponseDto(schedule1)).thenReturn(responseDto1);
        when(homeTaskMapper.toDto(homeTask1)).thenReturn(homeTaskResponseDto1);

        List<GetScheduleResponseDto> actual = scheduleService.getTeachersSchedule(requestDto);

        assertThat(actual).hasSize(1);
        assertThat(actual.get(0).tasks()).hasSize(1);
        assertThat(actual.get(0).tasks().get(0).scheduleId()).isEqualTo(1L);
    }

    @Test
    @DisplayName("Create students' schedule")
    public void createStudentsSchedule_ReturnsListOfScheduleResponseDto() {
        CreateScheduleDto scheduleDto1 = new CreateScheduleDto(1L, Schedule.Day.THURSDAY, 1L,
                LocalTime.of(9, 30), LocalTime.of(10, 15));
        CreateScheduleDto scheduleDto2 = new CreateScheduleDto(1L, Schedule.Day.FRIDAY, 2L,
                LocalTime.of(10, 30), LocalTime.of(11, 15));

        Schedule schedule1 = new Schedule();
        Schedule schedule2 = new Schedule();

        Class ourClass = new Class();
        Subject subject1 = new Subject();
        subject1.setId(1L);
        Subject subject2 = new Subject();
        subject2.setId(2L);
        List<Subject> subjects = new ArrayList<>();
        subjects.add(subject1);
        subjects.add(subject2);
        ourClass.setSubjects(subjects);

        ScheduleResponseDto responseDto1 = new ScheduleResponseDto(1L, 1L,
                Schedule.Day.THURSDAY, 1L,
                LocalTime.of(9, 30), LocalTime.of(10, 15));
        ScheduleResponseDto responseDto2 = new ScheduleResponseDto(2L, 1L, Schedule.Day.FRIDAY, 2L,
                LocalTime.of(10, 30), LocalTime.of(11, 15));

        when(classRepository.findClassById(any(Long.class))).thenReturn(Optional.of(ourClass));
        when(scheduleMapper.toEntity(scheduleDto1)).thenReturn(schedule1);
        when(scheduleMapper.toEntity(scheduleDto2)).thenReturn(schedule2);
        when(scheduleRepository.saveAll(List.of(schedule1, schedule2)))
                .thenReturn(List.of(schedule1, schedule2));
        when(scheduleMapper.toDto(schedule1)).thenReturn(responseDto1);
        when(scheduleMapper.toDto(schedule2)).thenReturn(responseDto2);

        List<ScheduleResponseDto> actual = scheduleService
                .createStudentsSchedule(List.of(scheduleDto1, scheduleDto2));

        assertThat(actual.get(0).day()).isEqualTo(Schedule.Day.THURSDAY);
        assertThat(actual.get(1).classId()).isEqualTo(1L);
    }

    @Test
    @DisplayName("Get students' schedule")
    public void getStudentsSchedule_ReturnsListOfGetScheduleResponseDto() {
        HomeTask homeTask1 = new HomeTask();
        homeTask1.setTaskDeadline(LocalDateTime.of(2024, 2, 12, 10, 30));
        HomeTask homeTask2 = new HomeTask();
        homeTask2.setTaskDeadline(LocalDateTime.of(2024, 2, 13, 11, 30));
        List<HomeTask> homeTasks = new ArrayList<>();
        homeTasks.add(homeTask1);
        homeTasks.add(homeTask2);
        Schedule schedule1 = new Schedule();
        schedule1.setId(1L);
        schedule1.setDay(Schedule.Day.MONDAY);
        schedule1.setHomeTasks(homeTasks);
        Schedule schedule2 = new Schedule();
        schedule2.setId(2L);
        schedule2.setDay(Schedule.Day.TUESDAY);

        HomeTaskResponseDto homeTaskResponseDto1 = new HomeTaskResponseDto(1L,
                "Read 1st text",
                LocalDateTime.of(2024, 2, 12, 10, 30),
                1L);
        GetScheduleResponseDto responseDto1 = new GetScheduleResponseDto(1L,
                1L, Schedule.Day.MONDAY, 1L,
                List.of(homeTaskResponseDto1),
                LocalTime.of(10, 30), LocalTime.of(11, 15));
        ScheduleRequestDto requestDto = new ScheduleRequestDto(1L, Schedule.Day.MONDAY,
                LocalDate.of(2024, 2, 12));

        when(scheduleRepository.findSchedulesByDayAndClassId(1L, Schedule.Day.MONDAY))
                .thenReturn(List.of(schedule1));
        when(scheduleMapper.toResponseDto(schedule1)).thenReturn(responseDto1);
        when(homeTaskMapper.toDto(homeTask1)).thenReturn(homeTaskResponseDto1);

        List<GetScheduleResponseDto> actual = scheduleService.getStudentsSchedule(requestDto);

        assertThat(actual).hasSize(1);
        assertThat(actual.get(0).tasks()).hasSize(1);
        assertThat(actual.get(0).tasks().get(0).scheduleId()).isEqualTo(1L);
    }
}
