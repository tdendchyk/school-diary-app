package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tdenchyk.schooldiaryapp.dto.subject.SubjectRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseWithTeacher;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.StudentRepository;
import tdenchyk.schooldiaryapp.service.SubjectService;

@Transactional
@Controller
@RequestMapping("/subjects")
@Tag(name = "MVC Subject Controller",
        description = "Views to browse and manage subjects")
@RequiredArgsConstructor
public class SubjectMvcController {
    private final SubjectService subjectService;
    private final StudentRepository studentRepository;

    @Operation(summary = "Get all subjects from DB")
    @GetMapping("/all")
    public String getAllSubjects(Model model) {
        List<SubjectResponseWithTeacher> subjects = subjectService.allSubjectsWithTeacherName();
        model.addAttribute("subjects", subjects);
        return "subject/subjects-list";
    }

    @Operation(summary = "Get subjects of a certain class",
            description = "Get all subjects by class's id")
    @GetMapping("my-class-subjects")
    public String getAllSubjectsOfClass(Model model,
                                        final Authentication authentication) {

        final User user = (User) authentication.getPrincipal();
        Student student = studentRepository.findByEmail(user.getEmail()).orElseThrow(
                () -> new EntityNotFoundException("Such user doesn't exist.")
        );
        List<SubjectResponseWithTeacher> subjects = subjectService.subjectWithTeacherNameBySubjectId(user);
        model.addAttribute("class", student.getClassNumberId().getNumber());
        model.addAttribute("subjects", subjects);
        return "subject/subjects-list-by-classid";
    }

    @Operation(summary = "Show form to add a new subject to DB")
    @GetMapping("/add")
    public String showFormToAddNewSubject(@ModelAttribute("subject") SubjectRequestDto requestDto) {
        return "subject/add-subject";
    }

    @Operation(summary = "Add a new subject to DB",
            description = "Teacher can add a subjectName of a new subject")
    @PostMapping("/add")
    public String addNewSubject(
            @ModelAttribute("subject") @Valid final SubjectRequestDto subjectRequest,
            BindingResult result) {
        if (result.hasErrors()) {
            return "subject/add-subject";
        }
        subjectService.addNewSubject(subjectRequest);
        return "redirect:/subjects/all";
    }

    @Operation(summary = "Delete a subject from DB",
            description = "Teacher can delete a subject from DB")
    @PostMapping("/delete/{subjectId}")
    public String deleteSubject(@PathVariable final Long subjectId) {
        subjectService.deleteSubject(subjectId);
        return "subject/subject-successfully-deleted";
    }
}
