package tdenchyk.schooldiaryapp.dto.schedule;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import tdenchyk.schooldiaryapp.model.Schedule;

public record ScheduleRequestDto(@NotNull(message = "You should choose it")
                                 Long classId,
                                 @NotNull(message = "You should choose it")
                                 Schedule.Day day,
                                 @NotNull(message = "You should choose it")
                                 LocalDate date) {
}
