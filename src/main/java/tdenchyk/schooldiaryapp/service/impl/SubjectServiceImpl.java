package tdenchyk.schooldiaryapp.service.impl;

import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.subject.SubjectRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseWithTeacher;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.SubjectMapper;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.ScheduleRepository;
import tdenchyk.schooldiaryapp.repository.SubjectRepository;
import tdenchyk.schooldiaryapp.service.SubjectService;

@Service
@Log4j2
@RequiredArgsConstructor
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    private final SubjectMapper subjectMapper;
    private final ScheduleRepository scheduleRepository;

    @Override
    public Subject findSubjectById(final Long id) {
        Subject subject;
        try {
            subject = subjectRepository.findById(id)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "Subject with id %d doesn't exist.".formatted(id))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find subject. Error: {}", e.getMessage());
            throw e;
        }
        return subject;
    }

    @Override
    public List<SubjectResponseDto> findAllSubjects() {
        return subjectRepository.findAll().stream()
                .map(subjectMapper::toDto)
                .toList();
    }

    @Transactional
    @Override
    public List<SubjectResponseDto> findAllByClassId(final User user) {
        Student student = (Student) user;
        return subjectRepository.findAllByClassNameId(student.getClassNumberId().getId())
                .stream()
                .map(subjectMapper::toDto)
                .toList();
    }

    @Override
    public SubjectResponseDto addNewSubject(final SubjectRequestDto requestDto) {
        Subject subject = subjectMapper.toSubject(requestDto);
        return subjectMapper.toDto(subjectRepository.save(subject));
    }

    @Transactional
    @Override
    public void deleteSubject(final Long id) {
        Subject subject;
        try {
            subject = subjectRepository.findById(id)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "Subject with the id %d doesn't exist".formatted(id))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to delete subject. Error {}", e.getMessage());
            throw e;
        }
        subject.getClasses().forEach(c -> c.getSubjects().remove(subject));
        List<Schedule> schedulesWithSubject = scheduleRepository.findAll().stream()
                .filter(s -> s.getSubject().getId().equals(id))
                .toList();
        scheduleRepository.deleteAll(schedulesWithSubject);
        subjectRepository.deleteById(id);
    }

    @Transactional
    @Override
    public List<SubjectResponseWithTeacher> subjectWithTeacherNameBySubjectId(final User user) {
        Student student = (Student) user;
        List<Subject> subjectsByClass = subjectRepository.findAllByClassNameId(student.getClassNumberId().getId());
        return addTeacherNamesToSubjects(subjectsByClass);
    }

    @Transactional
    @Override
    public List<SubjectResponseWithTeacher> allSubjectsWithTeacherName() {
        List<Subject> subjects = subjectRepository.findAll();
        return addTeacherNamesToSubjects(subjects);
    }

    public List<SubjectResponseWithTeacher> addTeacherNamesToSubjects(List<Subject> subjects) {
        List<SubjectResponseWithTeacher> subjectWithTeachers = new ArrayList<>();
        for (Subject subject : subjects) {
            if (subject.getTeacher() == null) {
                subjectWithTeachers.add(new SubjectResponseWithTeacher(subject.getId(), subject.getSubjectName(), ""));
            } else {
                subjectWithTeachers.add(new SubjectResponseWithTeacher(subject.getId(), subject.getSubjectName(),
                        subject.getTeacher().getFirstName() + " " + subject.getTeacher().getLastName()));
            }
        }
        return subjectWithTeachers;
    }
}
