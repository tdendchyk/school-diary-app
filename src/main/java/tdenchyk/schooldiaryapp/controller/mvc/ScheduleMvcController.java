package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.CreateScheduleDto;
import tdenchyk.schooldiaryapp.dto.schedule.GetScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.TeacherScheduleRequestDto;
import tdenchyk.schooldiaryapp.exception.AccessDeniedException;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.service.ClassService;
import tdenchyk.schooldiaryapp.service.ScheduleService;
import tdenchyk.schooldiaryapp.service.SubjectService;

@Controller
@RequiredArgsConstructor
@Tag(name = "MVC Schedule Controller",
        description = "Views to retrieve students' or teachers' schedule")
@RequestMapping("/schedules")
public class ScheduleMvcController {
    private final ScheduleService scheduleService;
    private final ClassService classService;
    private final SubjectService subjectService;

    @Operation(summary = "Form to get a schedule")
    @GetMapping(value = "/students")
    public String showFormToGetStudentsSchedule(@ModelAttribute("scheduleRequest") ScheduleRequestDto requestDto,
                                                Model model) {
        model.addAttribute("classes", classService.findAllClasses());
        return "schedule/student-schedule";
    }

    @Operation(summary = """
            Get schedule with hometasks for all students
            from a certain class for a certain day and date""")
    @PostMapping(value = "/students")
    public String getStudentsSchedule(@ModelAttribute("scheduleRequest") @Valid ScheduleRequestDto scheduleRequest,
                                      BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("classes", classService.findAllClasses());
            return "schedule/student-schedule";
        }
        List<String> subjects = scheduleService.getStudentsSchedule(scheduleRequest).stream()
                .map(GetScheduleResponseDto::subjectId)
                .map(subjectService::findSubjectById)
                .map(Subject::getSubjectName)
                .toList();
        model.addAttribute("subjects", subjects);
        model.addAttribute("classes", classService.findAllClasses());
        model.addAttribute("scheduleList", scheduleService.getStudentsSchedule(scheduleRequest));
        return "schedule/student-schedule";
    }

    @Operation(summary = "Form to get a certain schedule")
    @GetMapping("/teachers")
    public String showFormToGetTeachersSchedule(
            @ModelAttribute("request") TeacherScheduleRequestDto requestDto,
            Model model) {
        model.addAttribute("subjects", subjectService.findAllSubjects());
        model.addAttribute("classes", classService.findAllClasses());
        return "schedule/teacher-schedule";
    }

    @Operation(summary = """
            Get schedule with hometasks for teacher
            for a certain day and date""")
    @PostMapping("/teachers")
    public String getTeachersSchedule(
            @ModelAttribute("request") @Valid final TeacherScheduleRequestDto requestDto,
            BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("subjects", subjectService.findAllSubjects());
            return "schedule/teacher-schedule";
        }
        List<String> classes = scheduleService.getTeachersSchedule(requestDto).stream()
                .map(GetScheduleResponseDto::classId)
                .map(classService::findClassById)
                .map(Class::getNumber)
                .toList();
        model.addAttribute("subjects", subjectService.findAllSubjects());
        model.addAttribute("classes", classes);
        model.addAttribute("scheduleList", scheduleService.getTeachersSchedule(requestDto));
        model.addAttribute("hometaskRequest", new HomeTaskRequestDto(null, null, null));
        scheduleService.getTeachersSchedule(requestDto);
        return "schedule/teacher-schedule";
    }

    @Operation(summary = "Form to add new schedule for a certain class")
    @GetMapping("/students/create")
    public String showFormToCreateStudentsSchedule(@ModelAttribute("schedule") CreateScheduleDto scheduleDto,
                                                   Model model) {
        model.addAttribute("classes", classService.findAllClasses());
        model.addAttribute("subjects", subjectService.findAllSubjects());
        return "schedule/add-schedule";
    }

    @Operation(summary = "Teacher can create schedule for a certain class and day")
    @PostMapping("/students/create")
    public String createStudentsSchedule(
            @ModelAttribute("schedule") @Valid CreateScheduleDto scheduleDto,
            BindingResult result, Model model) {
        model.addAttribute("classes", classService.findAllClasses());
        model.addAttribute("subjects", subjectService.findAllSubjects());
        if (result.hasErrors()) {
            return "schedule/add-schedule";
        }

        try {
            scheduleService.createStudentsSchedule(scheduleDto);
            return "schedule/schedule-successfully-created";
        } catch (EntityNotFoundException ex) {
            model.addAttribute("errorMessage", "Class doesn't study this subject");
            return "schedule/add-schedule";
        } catch (AccessDeniedException ex) {
            model.addAttribute("errorMessage", "There is already schedule for this time slot.");
            return "schedule/add-schedule";
        }
    }

    @Operation(summary = "Teacher can delete teacher's or class's schedule by id") //TODO: add button to delete
    @PostMapping("/students/delete/{scheduleId}")
    public String deleteStudentsSchedule(@PathVariable final Long scheduleId) {
        scheduleService.deleteScheduleById(scheduleId);
        return "schedule/schedule-successfully-deleted";
    }
}
