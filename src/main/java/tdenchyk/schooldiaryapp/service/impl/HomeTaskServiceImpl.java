package tdenchyk.schooldiaryapp.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.exception.AccessDeniedException;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.HomeTaskMapper;
import tdenchyk.schooldiaryapp.model.HomeTask;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.HomeTaskRepository;
import tdenchyk.schooldiaryapp.repository.ScheduleRepository;
import tdenchyk.schooldiaryapp.service.HomeTaskService;
import tdenchyk.schooldiaryapp.service.ScheduleService;

@Service
@Log4j2
@RequiredArgsConstructor
public class HomeTaskServiceImpl implements HomeTaskService {
    private final HomeTaskRepository homeTaskRepository;
    private final ScheduleRepository scheduleRepository;
    private final HomeTaskMapper homeTaskMapper;
    private final ScheduleService scheduleService;

    @Override
    public HomeTaskResponseDto addHomeTask(HomeTaskRequestDto requestDto) {
        HomeTask homeTask = homeTaskMapper.toEntity(requestDto);
        Schedule schedule = scheduleService.findById(requestDto.scheduleId());
        homeTask.setSchedule(schedule);
        return homeTaskMapper.toDto(homeTaskRepository.save(homeTask));
    }

    @Transactional
    @Override
    public HomeTaskResponseDto addHomeTask(final User user, final HomeTaskRequestDto requestDto) {
        Teacher teacher = (Teacher) user;
        checkScheduleIdAndAuthorities(requestDto.scheduleId(), teacher);

        HomeTask homeTask = homeTaskMapper.toEntity(requestDto);
        Schedule schedule = scheduleService.findById(requestDto.scheduleId());
        homeTask.setSchedule(schedule);
        return homeTaskMapper.toDto(homeTaskRepository.save(homeTask));
    }

    @Transactional
    @Override
    public HomeTaskResponseDto updateHomeTask(final Long id,
                                              final User user,
                                              final HomeTaskRequestDto requestDto) {
        Teacher teacher = (Teacher) user;
        checkScheduleIdAndAuthorities(requestDto.scheduleId(), teacher);
        HomeTask homeTask;
        try {
            homeTask = homeTaskRepository.findById(id).orElseThrow(
                    () -> new EntityNotFoundException(
                            "Hometask with id %d doesn't exist".formatted(id))
            );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find hometask. Error: {}", e.getMessage());
            throw e;
        }
        homeTask.setTask(requestDto.task());
        return homeTaskMapper.toDto(homeTask);
    }

    @Override
    public HomeTaskResponseDto findById(final Long id) {
        HomeTask homeTask;
        try {
            homeTask = homeTaskRepository.findById(id).orElseThrow(
                    () -> new EntityNotFoundException(
                            "Hometask with id %d doesn't exist".formatted(id))
            );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find hometask. Error: {}", e.getMessage());
            throw e;
        }
        return homeTaskMapper.toDto(homeTask);
    }

    @Override
    public void deleteById(final Long id) {
        homeTaskRepository.deleteById(id);
        log.info("Successfully deleted hometask with id {}", id);
    }

    public void checkScheduleIdAndAuthorities(final Long scheduleId, final Teacher teacher) {
        Schedule scheduleOfDto;
        try {
            scheduleOfDto = scheduleRepository.findById(scheduleId).orElseThrow(
                    () -> new EntityNotFoundException(
                            "Schedule with id %d doesn't exist.".formatted(scheduleId))
            );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find schedule. Error: {}", e.getMessage());
            throw e;
        }
        if (!teacher.getSubject().getId().equals(scheduleOfDto.getSubject().getId())) {
            log.error("""
                    Failed to add new hometask.
                    Teacher with id {} is not responsible for schedule with id {}""",
                    teacher.getId(), scheduleId);
            throw new AccessDeniedException("Oops..you are not responsible for this subject.");
        }
    }
}
