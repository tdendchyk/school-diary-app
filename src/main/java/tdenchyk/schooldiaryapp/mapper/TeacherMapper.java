package tdenchyk.schooldiaryapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherResponseDto;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.service.SubjectService;

@Mapper(config = MapperConfig.class, uses = SubjectService.class)
public interface TeacherMapper {
    @Mapping(source = "subjectId", target = "subject")
    Teacher toTeacher(final TeacherRegisterRequestDto requestDto);

    @Mapping(source = "subject.id", target = "subjectId")
    TeacherRegisterResponseDto toDto(final Teacher teacher);

    @Mapping(source = "subject.id", target = "subjectId")
    TeacherResponseDto toResponseDto(final Teacher teacher);
}
