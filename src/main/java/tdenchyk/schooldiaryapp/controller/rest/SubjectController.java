package tdenchyk.schooldiaryapp.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tdenchyk.schooldiaryapp.dto.subject.SubjectRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.service.SubjectService;

@RestController
@Log4j2
@RequiredArgsConstructor
@Tag(name = "Subject Controller",
        description = """
                Endpoints to get all subjects,
                add or delete a subject""")
@RequestMapping("/api/subjects")
public class SubjectController {
    private final SubjectService subjectService;

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get all available subjects")
    @GetMapping
    public List<SubjectResponseDto> getAllSubjects() {
        return subjectService.findAllSubjects();
    }

    @PreAuthorize("hasRole('STUDENT')")
    @Operation(summary = "Get subjects of a certain class",
            description = "Get all subjects by class's id")
    @GetMapping("my-class-subjects")
    public List<SubjectResponseDto> getAllSubjectsOfClass(final Authentication authentication) {
        final User user = (User) authentication.getPrincipal();
        log.info("Student with id {} retrieves their subject", user.getId());
        return subjectService.findAllByClassId(user);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Add a new subject to DB",
            description = "Teacher can add a subjectName of a new subject")
    @PostMapping("/add")
    @ResponseStatus(value = HttpStatus.CREATED)
    public SubjectResponseDto addNewSubject(
            @RequestBody @Valid final SubjectRequestDto subjectRequest) {
        log.info("Adding new subject {}", subjectRequest.subjectName());
        return subjectService.addNewSubject(subjectRequest);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Delete a subject from DB",
            description = "Teacher can delete a subject from DB")
    @DeleteMapping("/delete/{subjectId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteSubject(@PathVariable final Long subjectId) {
        log.info("Deleting subject with id {}", subjectId);
        subjectService.deleteSubject(subjectId);
    }
}
