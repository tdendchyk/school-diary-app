package tdenchyk.schooldiaryapp.controller.rest;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.shaded.org.apache.commons.lang3.builder.EqualsBuilder;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserLoginRequestDto;
import tdenchyk.schooldiaryapp.dto.user.UserLoginResponseDto;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthControllerTest {
    protected static MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeAll
    static void beforeAll(@Autowired WebApplicationContext applicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @Sql(
            scripts = "classpath:/database/class/add-one-class.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = "classpath:/database/users/delete-all-users.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Sql(
            scripts = "classpath:/database/class/delete-all-classes.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @DisplayName("Register new student")
    public void registerStudent_ValidData_Success() throws Exception {
        StudentRegisterRequestDto requestDto = new StudentRegisterRequestDto("Mariana", "Volkova",
                "volkova@gmail.com", "password", "password", "389645151627",
                1L);
        String jsonRequest = objectMapper.writeValueAsString(requestDto);

        StudentRegisterResponseDto expected = new StudentRegisterResponseDto(1L, "Mariana", "Volkova",
                "volkova@gmail.com", "389645151627", 1L);

        MvcResult result = mockMvc.perform(
                        post("/api/auth/register/student")
                                .content(jsonRequest)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        StudentRegisterResponseDto actual = objectMapper.readValue(result.getResponse().getContentAsString(),
                StudentRegisterResponseDto.class);

        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.id());

        EqualsBuilder.reflectionEquals(expected, actual, "id", "password", "repeatPassword");
    }

    @Test
    @Sql(
            scripts = "classpath:/database/users/delete-all-users.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @DisplayName("Register new student")
    public void registerTeacher_ValidData_Success() throws Exception {
        TeacherRegisterRequestDto requestDto = new TeacherRegisterRequestDto("Mariana", "Volkova",
                "volkova@gmail.com", "password", "password", "389645151627", null);
        String jsonRequest = objectMapper.writeValueAsString(requestDto);

        TeacherRegisterResponseDto expected = new TeacherRegisterResponseDto(1L, "Mariana", "Volkova",
                "volkova@gmail.com", "389645151627", null);

        MvcResult result = mockMvc.perform(
                        post("/api/auth/register/teacher")
                                .content(jsonRequest)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        TeacherRegisterResponseDto actual = objectMapper.readValue(result.getResponse().getContentAsString(),
                TeacherRegisterResponseDto.class);

        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.id());

        EqualsBuilder.reflectionEquals(expected, actual, "id", "password", "repeatPassword");
    }

    @Test
    @Sql(
            scripts = "classpath:/database/users/add-user.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = "classpath:/database/users/delete-all-users.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @DisplayName("Login user")
    public void login_ValidData_Success() throws Exception {
        UserLoginRequestDto requestDto = new UserLoginRequestDto("valieva@gmail.com", "password");
        String jsonRequest = objectMapper.writeValueAsString(requestDto);

        MvcResult result = mockMvc.perform(
                        post("/api/auth/login")
                                .content(jsonRequest)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        UserLoginResponseDto actual = objectMapper.readValue(result.getResponse().getContentAsString(),
                UserLoginResponseDto.class);

        Assertions.assertNotNull(actual.jwt());
    }
}
