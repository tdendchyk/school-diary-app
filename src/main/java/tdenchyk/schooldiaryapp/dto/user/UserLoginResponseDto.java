package tdenchyk.schooldiaryapp.dto.user;

public record UserLoginResponseDto(String jwt) {
}
