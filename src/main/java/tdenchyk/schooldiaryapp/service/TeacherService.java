package tdenchyk.schooldiaryapp.service;

import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterResponseDto;

public interface TeacherService {
    TeacherRegisterResponseDto register(final TeacherRegisterRequestDto requestDto);
}
