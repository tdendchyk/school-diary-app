package tdenchyk.schooldiaryapp.service.impl;

import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.CreateScheduleDto;
import tdenchyk.schooldiaryapp.dto.schedule.GetScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.TeacherScheduleRequestDto;
import tdenchyk.schooldiaryapp.exception.AccessDeniedException;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.HomeTaskMapper;
import tdenchyk.schooldiaryapp.mapper.ScheduleMapper;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.repository.ClassRepository;
import tdenchyk.schooldiaryapp.repository.ScheduleRepository;
import tdenchyk.schooldiaryapp.service.ScheduleService;

@RequiredArgsConstructor
@Log4j2
@Service
public class ScheduleServiceImpl implements ScheduleService {
    private final ScheduleMapper scheduleMapper;
    private final HomeTaskMapper homeTaskMapper;
    private final ScheduleRepository scheduleRepository;
    private final ClassRepository classRepository;

    @Override
    public Schedule findById(Long id) {
        Schedule schedule;
        try {
            schedule = scheduleRepository.findById(id)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "Schedule with id %d doesn't exist".formatted(id))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find schedule. Error: {} ", e.getMessage());
            throw e;
        }
        return schedule;
    }

    @Transactional
    @Override
    public List<GetScheduleResponseDto> getTeachersSchedule(
            final TeacherScheduleRequestDto requestDto) {
        List<Schedule> schedules =
                scheduleRepository.findSchedulesBySubjectId(requestDto.subjectId())
                        .stream()
                        .filter(s -> s.getDay().name().equalsIgnoreCase(requestDto.day().name()))
                        .toList();
        List<GetScheduleResponseDto> responseDtos = schedules.stream()
                .map(scheduleMapper::toResponseDto)
                .toList();
        return addHomeTasksToSchedule(schedules, requestDto.date(), responseDtos);
    }

    @Transactional
    @Override
    public List<GetScheduleResponseDto> getStudentsSchedule(final ScheduleRequestDto requestDto) {
        List<Schedule> schedules = scheduleRepository
                .findSchedulesByDayAndClassId(requestDto.classId(), requestDto.day());
        List<GetScheduleResponseDto> responseDtos = schedules.stream()
                .map(scheduleMapper::toResponseDto)
                .toList();
        return addHomeTasksToSchedule(schedules, requestDto.date(), responseDtos);
    }

    @Transactional
    @Override
    public List<ScheduleResponseDto> createStudentsSchedule(
            final List<CreateScheduleDto> scheduleDtos) {
        checkIfScheduleHasValidSubjects(scheduleDtos);
        List<Schedule> schedules = scheduleDtos.stream()
                .map(scheduleMapper::toEntity)
                .toList();

        return scheduleRepository.saveAll(schedules).stream()
                .map(scheduleMapper::toDto)
                .toList();
    }

    @Transactional
    @Override
    public ScheduleResponseDto createStudentsSchedule(
            final CreateScheduleDto scheduleDto) {
        checkIfScheduleHasValidSubjects(List.of(scheduleDto));
        checkIfTimeIsAvailable(scheduleDto);
        Schedule schedule = scheduleMapper.toEntity(scheduleDto);
        return scheduleMapper.toDto(scheduleRepository.save(schedule));
    }

    public void checkIfTimeIsAvailable(CreateScheduleDto scheduleDto) {
        List<Schedule> schedules = scheduleRepository
                .findSchedulesByDayAndClassId(scheduleDto.classId(), scheduleDto.day());
        Optional<Schedule> matchInTime = schedules.stream()
                .filter(s -> scheduleDto.startTime().equals(s.getStartTime()))
                .findAny();
        if (matchInTime.isPresent()) {
            log.error("""
                    Failed to add schedule.
                    Start time {} is not available""", scheduleDto.startTime());
            throw new AccessDeniedException("There is already schedule for this time slot.");
        }
    }

    public void checkIfScheduleHasValidSubjects(List<CreateScheduleDto> scheduleDtos) {
        for (CreateScheduleDto scheduleDto : scheduleDtos) {
            Class foundClass = classRepository.findClassById(scheduleDto.classId()).orElseThrow(
                    () -> new EntityNotFoundException(
                            "Class with id %d doesn't exist".formatted(scheduleDto.classId())
                    ));
            Optional<Subject> subject = foundClass.getSubjects().stream()
                    .filter(s -> s.getId().equals(scheduleDto.subjectId()))
                    .findAny();
            if (subject.isEmpty()) {
                log.error("""
                                Failed to add schedule.
                                Class with id {} doesn't have subject with id {}""",
                        scheduleDto.classId(), scheduleDto.subjectId());
                throw new EntityNotFoundException(
                        "Class with id %d doesn't have subject with id %d"
                                .formatted(scheduleDto.classId(), scheduleDto.subjectId()));
            }
        }
    }

    @Override
    public void deleteScheduleById(final Long id) {
        scheduleRepository.deleteById(id);
        log.info("Successfully deleted schedule with id {}", id);
    }

    public List<GetScheduleResponseDto> addHomeTasksToSchedule(
            final List<Schedule> schedules,
            final LocalDate date,
            final List<GetScheduleResponseDto> responseDtos) {
        List<GetScheduleResponseDto> scheduleWithHomeTasks = new ArrayList<>();
        for (int i = 0; i < schedules.size(); i++) {
            List<HomeTaskResponseDto> filteredHomeTasks = schedules.get(i).getHomeTasks().stream()
                    .filter(h -> h.getTaskDeadline().toLocalDate().equals(date))
                    .map(homeTaskMapper::toDto)
                    .toList();
            scheduleWithHomeTasks.add(responseDtos.get(i).withTasks(filteredHomeTasks));
        }
        log.info("Hometasks added to schedules for date {}", date);
        return scheduleWithHomeTasks;
    }
}
