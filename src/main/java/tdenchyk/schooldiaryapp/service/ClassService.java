package tdenchyk.schooldiaryapp.service;

import java.util.List;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.ClassSubjectsUpdateDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;
import tdenchyk.schooldiaryapp.model.Class;

public interface ClassService {
    Class findClassById(final Long id);

    List<ClassResponseDto> findAllClasses();

    ClassResponseDto addNewClass(final CreateClassRequestDto requestDto);

    ClassResponseDto updateClassSubjects(final Long id, final ClassSubjectsUpdateDto updateDto);
}
