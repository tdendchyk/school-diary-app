package tdenchyk.schooldiaryapp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;

@Getter
@Setter
@SQLRestriction(value = "is_deleted=false")
@SQLDelete(sql = "UPDATE subjects SET is_deleted = true WHERE id=?")
@Entity
@Table(name = "subjects")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String subjectName;

    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "subject")
    private Teacher teacher;

    @ManyToMany(mappedBy = "subjects")
    private List<Class> classes = new ArrayList<>();

    @Column(nullable = false)
    private boolean isDeleted = false;
}
