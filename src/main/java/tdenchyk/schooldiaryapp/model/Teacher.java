package tdenchyk.schooldiaryapp.model;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "teachers")
public class Teacher extends User {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subject_id", unique = true)
    private Subject subject;

    @ManyToMany(mappedBy = "teachers")
    private List<Class> classes = new ArrayList<>();
}
