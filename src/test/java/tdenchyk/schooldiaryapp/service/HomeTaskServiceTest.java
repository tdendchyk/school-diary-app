package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.exception.AccessDeniedException;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.HomeTaskMapper;
import tdenchyk.schooldiaryapp.mapper.impl.HomeTaskMapperImpl;
import tdenchyk.schooldiaryapp.model.HomeTask;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.HomeTaskRepository;
import tdenchyk.schooldiaryapp.repository.ScheduleRepository;
import tdenchyk.schooldiaryapp.service.impl.HomeTaskServiceImpl;

@ExtendWith(MockitoExtension.class)
public class HomeTaskServiceTest {
    private static final HomeTask homeTask;
    private static final HomeTaskRequestDto requestDto;
    private static final HomeTaskResponseDto responseDto;
    @Mock
    private HomeTaskRepository homeTaskRepository;
    @Mock
    private ScheduleRepository scheduleRepository;
    @Spy
    private HomeTaskMapper homeTaskMapper = new HomeTaskMapperImpl();
    @Mock
    private ScheduleService scheduleService;
    @InjectMocks
    private HomeTaskServiceImpl homeTaskService;

    static {
        homeTask = new HomeTask();
        homeTask.setId(1L);
        homeTask.setTask("Read paragraph 2");
        homeTask.setTaskDeadline(LocalDateTime.of(2024, 1, 20, 10, 30));

        requestDto = new HomeTaskRequestDto("Read paragraph 2",
                LocalDateTime.of(2024, 1, 20, 10, 30),
                1L);

        responseDto = new HomeTaskResponseDto(1L, "Read paragraph 2",
                LocalDateTime.of(2024, 1, 20, 10, 30),
                1L);
    }

    @Test
    @DisplayName("Add new hometask")
    public void addHomeTask_ReturnsHomeTaskResponseDto() {
        User user = new Teacher();
        Teacher teacher = (Teacher) user;
        Schedule schedule = new Schedule();
        Subject subject = new Subject();
        subject.setId(2L);
        teacher.setSubject(subject);
        schedule.setId(1L);
        schedule.setSubject(subject);

        when(homeTaskMapper.toEntity(requestDto)).thenReturn(homeTask);
        when(scheduleService.findById(1L)).thenReturn(schedule);
        when(scheduleRepository.findById(any(Long.class))).thenReturn(Optional.of(schedule));
        when(homeTaskRepository.save(homeTask)).thenReturn(homeTask);
        when(homeTaskMapper.toDto(homeTask)).thenReturn(responseDto);

        HomeTaskResponseDto actual = homeTaskService.addHomeTask(user, requestDto);

        assertThat(actual.task()).isEqualTo("Read paragraph 2");
    }

    @Test
    @DisplayName("Fail on adding hometask to schedule that user is not responsible for")
    public void addHomeTask_InvalidScheduleId_ThrowsAccessDeniedException() {
        Teacher teacher = new Teacher();
        Subject subject1 = new Subject();
        subject1.setId(2L);
        Subject subject2 = new Subject();
        subject2.setId(3L);
        Schedule schedule = new Schedule();
        teacher.setSubject(subject1);
        schedule.setSubject(subject2);

        when(scheduleRepository.findById(any(Long.class))).thenReturn(Optional.of(schedule));

        assertThrows(AccessDeniedException.class,
                () -> homeTaskService.addHomeTask(teacher, requestDto));
    }

    @Test
    @DisplayName("Update hometask by id")
    public void updateHomeTask_ValidId_ReturnsHomeTaskResponseDto() {
        Teacher teacher = new Teacher();
        Schedule schedule = new Schedule();
        schedule.setId(1L);
        Subject subject = new Subject();
        subject.setId(2L);
        teacher.setSubject(subject);
        schedule.setSubject(subject);
        HomeTaskRequestDto requestDto = new HomeTaskRequestDto("Read paragraph 3",
                LocalDateTime.of(2024, 1, 20, 10, 30),
                1L);

        when(scheduleRepository.findById(any(Long.class))).thenReturn(Optional.of(schedule));
        when(homeTaskRepository.findById(any(Long.class))).thenReturn(Optional.of(homeTask));

        HomeTaskResponseDto actual = homeTaskService.updateHomeTask(1L, teacher, requestDto);

        assertThat(actual.task()).isEqualTo("Read paragraph 3");
    }

    @Test
    @DisplayName("Fail on updating hometask by invalid id")
    public void updateHomeTask_InvalidId_ThrowsEntityNotFoundException() {
        Teacher teacher = new Teacher();
        Schedule schedule = new Schedule();
        schedule.setId(1L);
        Subject subject = new Subject();
        subject.setId(2L);
        teacher.setSubject(subject);
        schedule.setSubject(subject);
        HomeTaskRequestDto requestDto = new HomeTaskRequestDto("Read paragraph 3",
                LocalDateTime.of(2024, 1, 20, 10, 30),
                1L);

        when(scheduleRepository.findById(any(Long.class))).thenReturn(Optional.of(schedule));
        when(homeTaskRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> homeTaskService.updateHomeTask(1L, teacher, requestDto));
    }

    @Test
    @DisplayName("Find hometask by id")
    public void findById_ValidId_ReturnsHomeTaskResponseDto() {
        when(homeTaskRepository.findById(any(Long.class))).thenReturn(Optional.of(homeTask));
        when(homeTaskMapper.toDto(homeTask)).thenReturn(responseDto);

        HomeTaskResponseDto actual = homeTaskService.findById(1L);

        assertThat(actual.id()).isEqualTo(1L);
        assertThat(actual.task()).isEqualTo("Read paragraph 2");
    }

    @Test
    @DisplayName("Fail on finding hometask by invalid id")
    public void findById_InvalidId_ThrowsEntityNotFoundException() {
        when(homeTaskRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> homeTaskService.findById(1L));
    }
}
