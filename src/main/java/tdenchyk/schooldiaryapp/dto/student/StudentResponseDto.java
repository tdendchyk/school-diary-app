package tdenchyk.schooldiaryapp.dto.student;

public record StudentResponseDto(Long id,
                                 String firstName,
                                 String lastName,
                                 String phoneNumber) {
}
