package tdenchyk.schooldiaryapp.controller.mvc;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import jakarta.transaction.Transactional;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tdenchyk.schooldiaryapp.controller.rest.AuthController;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.repository.ClassRepository;
import tdenchyk.schooldiaryapp.service.SubjectService;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserMvcControllerTest {

    protected static MockMvc mockMvc;

    @Autowired
    private AuthController authController;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private ClassRepository classRepository;

    @BeforeAll
    static void beforeAll(@Autowired WebApplicationContext applicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Order(1)
    @Sql(
            scripts = "classpath:/database/class/add-one-class.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/users/delete-all-users.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Get student by valid id")
    public void getStudentById_ValidId_Success() throws Exception {
        authController.registerStudent(new StudentRegisterRequestDto(
                "Oleg", "Moroz",
                "oleg.moroz@gmail.com", "password",
                "password", "3809975363416",
                1L));

        mockMvc.perform(get("/users/students/{id}", 1L))
                .andExpect(view().name("user/student-info"))
                .andExpect(model().attributeExists("student"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Order(2)
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/users/delete-all-users.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Get teacher by valid id")
    public void getTeacherById_ValidId_Success() throws Exception {
        authController.registerTeacher(new TeacherRegisterRequestDto(
                "Oleg", "Moroz",
                "oleg.moroz@gmail.com", "password",
                "password", "3809975363416", null));

        mockMvc.perform(get("/users/teachers/{id}", 2L))
                .andExpect(view().name("user/teacher-info"))
                .andExpect(model().attributeExists("teacher"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Order(3)
    @Test
    @DisplayName("Get all students")
    public void getAllStudents_Success() throws Exception {
        mockMvc.perform(get("/users/students/all"))
                .andExpect(view().name("user/all-students"))
                .andExpect(model().attributeExists("students"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Order(4)
    @Test
    @DisplayName("Get all students")
    public void getAllTeachers_Success() throws Exception {
        mockMvc.perform(get("/users/teachers/all"))
                .andExpect(view().name("user/all-teachers"))
                .andExpect(model().attributeExists("teachers"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Order(5)
    @Sql(
            scripts = "classpath:/database/class/add-one-class.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/users/delete-all-users.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Get students by class id")
    public void getAllStudentsByClassId_Success() throws Exception {
        authController.registerStudent(new StudentRegisterRequestDto(
                "Maria", "Horobenko",
                "maria.horobenko@gmail.com", "password",
                "password", "380997500416",
                1L));
        authController.registerStudent(new StudentRegisterRequestDto(
                "Volodymyr", "Ivachenko",
                "Volodymyr.Ivachenko@gmail.com", "password",
                "password", "380987500416",
                1L));
        mockMvc.perform(get("/users/classes/{classId}/students", 1L))
                .andExpect(view().name("user/students-by-classid"))
                .andExpect(model().attributeExists("students"))
                .andExpect(model().attributeExists("class"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Order(6)
    @Sql(
            scripts = {"classpath:/database/subjects/add-three-default-subjects.sql",
                    "classpath:/database/users/delete-all-users.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/users/delete-all-users.sql",
                    "classpath:/database/subjects/delete-all-subjects.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Get teachers by class id")
    public void getAllTeachersByClassId_Success() throws Exception {
        authController.registerTeacher(new TeacherRegisterRequestDto(
                "Vitaliy", "Vasilchenko",
                "vit.Vasilchenko@gmail.com", "password",
                "password", "380990500416", 1L));
        authController.registerTeacher(new TeacherRegisterRequestDto(
                "Roman", "Ivachenko",
                "rom.Ivachenko@gmail.com", "password",
                "password", "380937500416",
                2L));
        Class newClass = new Class();
        newClass.setNumber("11");
        newClass.setSubjects(List.of(subjectService.findSubjectById(1L), subjectService.findSubjectById(2L)));
        classRepository.save(newClass);
        mockMvc.perform(get("/users/classes/{classId}/teachers", 1L))
                .andExpect(view().name("user/teachers-by-classid"))
                .andExpect(model().attributeExists("teachers"))
                .andExpect(model().attributeExists("class"));
    }
}
