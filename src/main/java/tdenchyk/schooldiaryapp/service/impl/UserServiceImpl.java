package tdenchyk.schooldiaryapp.service.impl;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.student.StudentResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserUpdateRequestDto;
import tdenchyk.schooldiaryapp.exception.AccessDeniedException;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.StudentMapper;
import tdenchyk.schooldiaryapp.mapper.TeacherMapper;
import tdenchyk.schooldiaryapp.mapper.UserMapper;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Role;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.StudentRepository;
import tdenchyk.schooldiaryapp.repository.TeacherRepository;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.ClassService;
import tdenchyk.schooldiaryapp.service.UserService;

@RequiredArgsConstructor
@Log4j2
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final StudentRepository studentRepository;
    private final TeacherRepository teacherRepository;
    private final StudentMapper studentMapper;
    private final TeacherMapper teacherMapper;
    private final UserMapper userMapper;
    private final ClassService classService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<StudentResponseDto> findAllStudents() {
        return studentRepository.findAll()
                .stream()
                .map(studentMapper::toResponseDto)
                .toList();
    }

    @Override
    public List<TeacherResponseDto> findAllTeachers() {
        return teacherRepository.findAll()
                .stream()
                .map(teacherMapper::toResponseDto)
                .toList();
    }

    @Override
    public StudentResponseDto findStudentById(final Long id) {
        StudentResponseDto studentResponseDto;
        try {
            studentResponseDto = studentRepository.findById(id)
                    .map(studentMapper::toResponseDto)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "Student with id %d doesn't exist".formatted(id)
                            )
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find student. Error: {}", e.getMessage());
            throw e;
        }
        return studentResponseDto;
    }

    @Override
    public TeacherResponseDto findTeacherById(final Long id) {
        TeacherResponseDto teacherResponseDto;
        try {
            teacherResponseDto = teacherRepository.findById(id)
                    .map(teacherMapper::toResponseDto)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "Teacher with id %d doesn't exist".formatted(id))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find teacher. Error: {}", e.getMessage());
            throw e;
        }
        return teacherResponseDto;
    }

    @Override
    public List<StudentResponseDto> findAllStudentsByClassId(final Long id) {
        classService.findClassById(id);
        return studentRepository.findAllByClassId(id)
                .stream()
                .map(studentMapper::toResponseDto)
                .toList();
    }

    @Override
    public List<TeacherResponseDto> findAllTeachersByClassId(final Long id) {
        Class classNumber = classService.findClassById(id);
        return classNumber.getTeachers().stream()
                .map(teacherMapper::toResponseDto)
                .toList();
    }

    @Transactional
    @Override
    public void deleteStudentById(final Long id) {
        User user;
        try {
            user = userRepository.findById(id)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "User with id %d doesn't exist".formatted(id))
                    );

        } catch (EntityNotFoundException e) {
            log.error("Failed to delete student. Error: {}", e.getMessage());
            throw e;
        }
        checkRole(user.getRoles(), Role.RoleName.STUDENT);
        userRepository.deleteById(id);
        log.info("Successfully deleted student with id {}", id);
    }

    @Transactional
    @Override
    public void deleteTeacherById(final Long id) {
        User user;
        try {
            user = userRepository.findById(id)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "User with id %d doesn't exist".formatted(id))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to delete teacher. Error: {}", e.getMessage());
            throw e;
        }
        checkRole(user.getRoles(), Role.RoleName.TEACHER);
        userRepository.deleteById(id);
        log.info("Successfully deleted teacher with id {}", id);
    }

    @Override
    public UserResponseDto updateUser(final User user,
                                      final UserUpdateRequestDto requestDto,
                                      final Long userId) {
        User foundUser;
        try {
            foundUser = userRepository.findById(userId)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "User with id %d doesn't exist".formatted(userId))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find user. Error: {}", e.getMessage());
            throw e;
        }
        if (!foundUser.getEmail().equals(user.getEmail())) {
            log.error("""
                            Failed to update account with email {}.
                            User with email {} isn't allowed to do that""",
                    foundUser.getEmail(), user.getEmail());
            throw new AccessDeniedException("Oops...you are not responsible for this account.");
        }
        userMapper.updateUser(requestDto, user);
        User saved = userRepository.save(user);
        log.info("Successfully updated info on user with id {}", user.getId());
        return userMapper.toResponseDto(saved);
    }

    @Override
    public UserResponseDto updateUser(User user, UserUpdateRequestDto requestDto) {
        userMapper.updateUser(requestDto, user);
        User saved = userRepository.save(user);
        log.info("Successfully updated info on user with id {}", user.getId());
        return userMapper.toResponseDto(saved);
    }

    private void checkRole(final Set<Role> roles, final Role.RoleName roleName) {
        Optional<Role.RoleName> role = roles.stream()
                .map(Role::getName)
                .filter(r -> r.equals(roleName))
                .findAny();
        if (role.isEmpty()) {
            log.error("Failed to delete. Such %s doesn't exist"
                    .formatted(roleName.name().toLowerCase()));
            throw new EntityNotFoundException(
                    "Such %s doesn't exist".formatted(roleName.name().toLowerCase()));
        }
    }
}
