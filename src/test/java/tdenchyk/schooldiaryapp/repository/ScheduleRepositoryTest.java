package tdenchyk.schooldiaryapp.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Schedule;
import tdenchyk.schooldiaryapp.model.Subject;

@DataJpaTest
public class ScheduleRepositoryTest {
    @Autowired
    private ScheduleRepository scheduleRepository;
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private SubjectRepository subjectRepository;

    @Test
    @DisplayName("Find schedules by day and class id")
    public void findSchedulesByDayAndClassId_ReturnsListOfSchedule() {
        Class class1 = new Class();
        class1.setNumber("10");
        Class class2 = new Class();
        class2.setNumber("11");
        classRepository.saveAll(List.of(class1, class2));
        Subject subject1 = new Subject();
        subject1.setSubjectName("Maths");
        Subject subject2 = new Subject();
        subject2.setSubjectName("Geography");
        Subject subject3 = new Subject();
        subject3.setSubjectName("History");
        subjectRepository.saveAll(List.of(subject1, subject2, subject3));

        Schedule schedule1 = new Schedule();
        schedule1.setSchoolClass(class1);
        schedule1.setDay(Schedule.Day.MONDAY);
        schedule1.setSubject(subject1);
        schedule1.setStartTime(LocalTime.of(9, 30));
        schedule1.setEndTime(LocalTime.of(10, 15));
        scheduleRepository.save(schedule1);
        Schedule schedule2 = new Schedule();
        schedule2.setSchoolClass(class1);
        schedule2.setDay(Schedule.Day.THURSDAY);
        schedule2.setSubject(subject2);
        schedule2.setStartTime(LocalTime.of(9, 30));
        schedule2.setEndTime(LocalTime.of(10, 15));
        scheduleRepository.save(schedule2);
        Schedule schedule3 = new Schedule();
        schedule3.setSchoolClass(class2);
        schedule3.setDay(Schedule.Day.MONDAY);
        schedule3.setSubject(subject3);
        schedule3.setStartTime(LocalTime.of(9, 30));
        schedule3.setEndTime(LocalTime.of(10, 15));
        scheduleRepository.save(schedule3);

        List<Schedule> actual = scheduleRepository.findSchedulesByDayAndClassId(
                class1.getId(), Schedule.Day.MONDAY);

        assertThat(actual).isNotEmpty();
        assertThat(actual).hasSize(1);
        assertThat(actual.get(0).getSubject().getSubjectName()).isEqualTo("Maths");
    }
}
