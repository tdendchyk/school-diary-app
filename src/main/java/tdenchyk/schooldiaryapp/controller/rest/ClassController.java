package tdenchyk.schooldiaryapp.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.ClassSubjectsUpdateDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;
import tdenchyk.schooldiaryapp.service.ClassService;

@RequiredArgsConstructor
@RestController
@Log4j2
@Tag(name = "Class Controller",
        description = "Endpoints to add new class and retrieve all from DB")
@RequestMapping("/api/classes")
public class ClassController {
    private final ClassService classService;

    @Operation(summary = "Retrieve all classes with subjects' ids from DB")
    @PreAuthorize("hasRole('TEACHER')")
    @GetMapping
    public List<ClassResponseDto> getAllClasses() {
        return classService.findAllClasses();
    }

    @Operation(summary = "Add new class to DB")
    @PreAuthorize("hasRole('TEACHER')")
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/add")
    public ClassResponseDto addNewClass(
            @RequestBody @Valid final CreateClassRequestDto requestDto) {
        log.info("Adding class {} to DB", requestDto.number());
        return classService.addNewClass(requestDto);
    }

    @Operation(summary = "Update subjects for a certain class")
    @PreAuthorize("hasRole('TEACHER')")
    @PatchMapping("/{id}/update")
    public ClassResponseDto updateSubjects(
            @RequestBody @Valid final ClassSubjectsUpdateDto updateDto,
            @PathVariable final Long id) {
        log.info("Updating subjects of class with id {}", id);
        return classService.updateClassSubjects(id, updateDto);
    }
}
