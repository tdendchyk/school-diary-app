package tdenchyk.schooldiaryapp.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tdenchyk.schooldiaryapp.dto.schedule.CreateScheduleDto;
import tdenchyk.schooldiaryapp.dto.schedule.GetScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.TeacherScheduleRequestDto;
import tdenchyk.schooldiaryapp.service.ScheduleService;

@RestController
@RequiredArgsConstructor
@Tag(name = "Schedule Controller",
        description = "Endpoints to create and get student's and teacher's schedule")
@RequestMapping("/api/schedules")
public class ScheduleController {
    private final ScheduleService scheduleService;

    @PreAuthorize("hasRole('STUDENT')")
    @Operation(summary = """
            Get schedule with hometasks for all students
            from a certain class for a certain day and date""")
    @GetMapping("/students")
    public List<GetScheduleResponseDto> getStudentsSchedule(
            @RequestBody @Valid ScheduleRequestDto requestDto) {
        return scheduleService.getStudentsSchedule(requestDto);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = """
            Get schedule with hometasks for teacher
            for a certain day and date""")
    @GetMapping("/teachers")
    public List<GetScheduleResponseDto> getTeachersSchedule(
            @RequestBody @Valid final TeacherScheduleRequestDto requestDto) {
        return scheduleService.getTeachersSchedule(requestDto);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Teacher can create schedule for a certain class and day")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/students/create")
    public List<ScheduleResponseDto> createStudentsSchedule(
            @RequestBody @Valid final List<CreateScheduleDto> scheduleDtos) {
        return scheduleService.createStudentsSchedule(scheduleDtos);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Teacher can delete teacher's or class's schedule by id")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("/students/delete/{scheduleId}")
    public void deleteStudentsSchedule(@PathVariable final Long scheduleId) {
        scheduleService.deleteScheduleById(scheduleId);
    }
}
