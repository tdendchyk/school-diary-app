package tdenchyk.schooldiaryapp.dto.student;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import tdenchyk.schooldiaryapp.validator.annotation.PasswordMatch;

@PasswordMatch
public record StudentRegisterRequestDto(@NotBlank(message = "First name should contain at least 1 letter")
                                        @Length(max = 200, message = "First name should contain less than 200 symbols")
                                        String firstName,
                                        @NotBlank(message = "Last name should contain at least 1 letter")
                                        @Length(max = 200, message = "Last name should contain less than 200 symbols")
                                        String lastName,
                                        @NotBlank(message = "You can't leave it empty")
                                        @Email(message = "Email should contain @ symbol")
                                        @Length(max = 255, message = "Email should contain less than 255 symbols")
                                        String email,
                                        @NotBlank(message = "You can't leave it empty")
                                        @Length(min = 7, max = 50, message = "Password should be from 7 to 50 symbols long")
                                        String password,
                                        @NotBlank(message = "You can't leave it empty")
                                        @Length(min = 7, max = 50, message = "Password should be from 7 to 50 symbols long")
                                        String repeatPassword,
                                        @NotBlank(message = "You can't leave it empty")
                                        String phoneNumber,
                                        @NotNull(message = "You should choose something")
                                        Long classNumberId) {
}
