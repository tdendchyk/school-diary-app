package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.user.UserLoginRequestDto;
import tdenchyk.schooldiaryapp.exception.RegistrationException;
import tdenchyk.schooldiaryapp.security.AuthenticationService;
import tdenchyk.schooldiaryapp.service.ClassService;
import tdenchyk.schooldiaryapp.service.StudentService;
import tdenchyk.schooldiaryapp.service.SubjectService;
import tdenchyk.schooldiaryapp.service.TeacherService;

@Log4j2
@Controller
@RequiredArgsConstructor
@Tag(name = "MVC Authentication Controller",
        description = "Forms to register and login")
@RequestMapping
public class AuthMvcController {
    private final StudentService studentService;
    private final TeacherService teacherService;
    private final ClassService classService;
    private final SubjectService subjectService;
    private final AuthenticationService authenticationService;

    @Operation(summary = "Show form to register new student")
    @GetMapping("/register/new-student")
    public String showStudentRegistrationForm(
            @ModelAttribute("student") StudentRegisterRequestDto request,
            Model model) {
        List<ClassResponseDto> allClasses = classService.findAllClasses();
        model.addAttribute("classes", allClasses);
        return "auth/student-register-form";
    }

    @Operation(summary = "Register",
            description = "Register new student")
    @PostMapping("/register/new-student")
    public String registerStudent(
            @ModelAttribute("student") @Valid final StudentRegisterRequestDto request,
            BindingResult result, Model model) {
        log.info("User with email {} registers as student", request.email());
        if (result.hasErrors()) {
            List<ClassResponseDto> allClasses = classService.findAllClasses();
            model.addAttribute("classes", allClasses);
            model.addAttribute("passwordMessage", "Passwords should match");
            return "auth/student-register-form";
        }
        try {
            studentService.register(request);
            log.info("User with email {} is successfully registered as student", request.email());
            return "auth/successful-registration";
        } catch (RegistrationException ex) {
            log.info("Failed. User with email {} already exists", request.email());
            model.addAttribute("emailMessage", "User already exists");
            return "auth/student-register-form";
        }
    }

    @Operation(summary = "Show form to register new teacher")
    @GetMapping("/register/new-teacher")
    public String showTeacherRegistrationForm(
            @ModelAttribute("teacher") TeacherRegisterRequestDto request,
            Model model) {
        model.addAttribute("subjects", subjectService.findAllSubjects());
        return "auth/teacher-register-form";
    }

    @Operation(summary = "Register",
            description = "Register new teacher")
    @PostMapping("/register/new-teacher")
    public String registerTeacher(
            @ModelAttribute("teacher") @Valid final TeacherRegisterRequestDto requestDto,
            BindingResult result, Model model) {
        log.info("User with email {} registers as teacher", requestDto.email());
        if (result.hasErrors()) {
            List<SubjectResponseDto> allSubjects = subjectService.findAllSubjects();
            model.addAttribute("passwordMessage", "Passwords should match");
            model.addAttribute("subjects", allSubjects);
            return "auth/teacher-register-form";
        }

        try {
            teacherService.register(requestDto);
            log.info("User with email {} is successfully registered as teacher",
                    requestDto.email());
            return "auth/successful-registration";
        } catch (RegistrationException ex) {
            log.info("Failed. User with email {} already exists", requestDto.email());
            model.addAttribute("emailMessage", "User already exists");
            return "auth/teacher-register-form";
        }
    }

    @Operation(summary = "Show form to login a user")
    @GetMapping("/login")
    public String showLoginForm(@ModelAttribute("user") UserLoginRequestDto requestDto) {
        return "auth/login-form";
    }

    @Operation(summary = "Login",
            description = "Login user by their credentials")
    @PostMapping("/login")
    public String login(
            @ModelAttribute("user") @Valid final UserLoginRequestDto requestDto,
            BindingResult result) {
        if (result.hasErrors()) {
            return "auth/login-form";
        }
        try {
            authenticationService.authenticate(requestDto);
            return "start/about";
        } catch (RuntimeException ex) {
            return "auth/login-form";
        }
    }
}
