package tdenchyk.schooldiaryapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterResponseDto;
import tdenchyk.schooldiaryapp.dto.student.StudentResponseDto;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.service.ClassService;

@Mapper(config = MapperConfig.class, uses = ClassService.class)
public interface StudentMapper {
    @Mapping(source = "classNumberId.id", target = "classNumberId")
    StudentRegisterResponseDto toDto(final Student student);

    StudentResponseDto toResponseDto(final Student student);

    @Mapping(source = "classNumberId", target = "classNumberId")
    Student toStudent(final StudentRegisterRequestDto requestDto);
}
