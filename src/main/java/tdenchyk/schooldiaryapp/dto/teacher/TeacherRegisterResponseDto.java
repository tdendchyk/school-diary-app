package tdenchyk.schooldiaryapp.dto.teacher;

public record TeacherRegisterResponseDto(Long id,
                                         String firstName,
                                         String lastName,
                                         String email,
                                         String phoneNumber,
                                         Long subjectId) {
}
