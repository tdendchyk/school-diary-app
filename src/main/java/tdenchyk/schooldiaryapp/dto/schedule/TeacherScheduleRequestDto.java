package tdenchyk.schooldiaryapp.dto.schedule;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import tdenchyk.schooldiaryapp.model.Schedule;

public record TeacherScheduleRequestDto(@NotNull(message = "You should choose it")
                                        Long subjectId,
                                        @NotNull(message = "You should choose it")
                                        Schedule.Day day,
                                        @NotNull(message = "You should choose it")
                                        LocalDate date) {
}
