package tdenchyk.schooldiaryapp.dto.hometask;

import java.time.LocalDateTime;

public record HomeTaskResponseDto(Long id,
                                  String task,
                                  LocalDateTime taskDeadline,
                                  Long scheduleId) {
}
