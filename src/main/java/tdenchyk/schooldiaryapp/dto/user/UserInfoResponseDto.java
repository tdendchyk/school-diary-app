package tdenchyk.schooldiaryapp.dto.user;

public record UserInfoResponseDto(String firstName,
                                  String lastName,
                                  String email,
                                  String password,
                                  String phoneNumber) {
}
