##  Your Diary
*📓 Online School Diary*
<hr>

### Introduction 

*Welcome to Your Diary!* 👋
<br>

This app is created to assist both teachers and students. 
Here educational institutions can ... 
- see all their teachers and students;
- add new grades with subjects they study;
- add new subjects separately;
- create schedules for students;
- see schedules both for teachers and for students;

  **<u>And the major feature:</u>**
- It is way easier to assign hometask with this app. **Teacher can
assign hometask in their schedule and it will be automatically added
to students' schedule.** 


### Technologies used:
* **Java 17**
* **Spring boot**
* **Spring Data JPA**
* **Spring Security**
* **Spring MVC**
* **MySQL**
* **Swagger**
* **Lombok**
* **MapStruct**
* **Docker**
* **Liquibase**

> ### How to launch the application?
> 1. Make sure you have JDK and Docker installed
> 2. Clone the repository from GitLab
> 3. Create .env file and fill in all necessary variables (You can find them in .env.sample)
> 4. Run the command mvn clean package
> 5. Run `docker-compose up` to build and start the Docker containers
> 6. The application will be running at http://localhost:8081
> 7. Swagger is available at http://localhost:8081/swagger-ui/index.html


