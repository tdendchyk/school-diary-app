package tdenchyk.schooldiaryapp.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Student;

@DataJpaTest
public class StudentRepositoryTest {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ClassRepository classRepository;

    @Test
    @DisplayName("Find all students by class id")
    public void findAllByClassId_ReturnsListOfStudents() {
        Class class1 = new Class();
        class1.setNumber("10");
        Class class2 = new Class();
        class2.setNumber("11");
        classRepository.saveAll(List.of(class1, class2));

        Student student1 = new Student();
        student1.setId(1L);
        student1.setFirstName("Alex");
        student1.setLastName("Green");
        student1.setEmail("aGreen@gmail.com");
        student1.setPassword("password");
        student1.setPhoneNumber("9874562111");
        student1.setClassNumberId(class1);
        Student student2 = new Student();
        student2.setId(2L);
        student2.setFirstName("Sofia");
        student2.setLastName("Movchun");
        student2.setEmail("sofia.movchun@gmail.com");
        student2.setPassword("password");
        student2.setPhoneNumber("9874562999");
        student2.setClassNumberId(class2);
        Student student3 = new Student();
        student3.setId(3L);
        student3.setFirstName("Volodymyr");
        student3.setLastName("Redkin");
        student3.setEmail("redkin.volodymyr@gmail.com");
        student3.setPassword("password");
        student3.setPhoneNumber("9874562000");
        student3.setClassNumberId(class1);

        studentRepository.saveAll(List.of(student1, student2, student3));
        List<Student> actual = studentRepository.findAllByClassId(class1.getId());

        assertThat(actual).hasSize(2);
        assertThat(actual.get(0).getFirstName()).isEqualTo("Alex");
        assertThat(actual.get(1).getFirstName()).isEqualTo("Volodymyr");
    }
}
