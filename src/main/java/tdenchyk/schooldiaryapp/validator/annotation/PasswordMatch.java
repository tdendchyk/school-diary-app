package tdenchyk.schooldiaryapp.validator.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import tdenchyk.schooldiaryapp.validator.StudentFieldMatchValidator;
import tdenchyk.schooldiaryapp.validator.TeacherFieldMatchValidator;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {TeacherFieldMatchValidator.class, StudentFieldMatchValidator.class})
public @interface PasswordMatch {
    String message() default "Passwords don't match";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
