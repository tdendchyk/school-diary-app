package tdenchyk.schooldiaryapp.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Subject;

@Mapper(config = MapperConfig.class)
public interface ClassMapper {
    @Mapping(source = "subjectsId", target = "subjects", ignore = true)
    Class toClass(final CreateClassRequestDto requestDto);

    @Mapping(source = "subjects", target = "subjectsId", qualifiedByName = "subjectsToId")
    ClassResponseDto toDto(final Class classNumber);

    @Named("subjectsToId")
    default List<Long> subjectToId(List<Subject> subjects) {
        return subjects.stream()
                .map(Subject::getId)
                .toList();
    }
}
