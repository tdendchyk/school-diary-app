package tdenchyk.schooldiaryapp.dto.classes;

import java.util.List;

public record ClassSubjectsUpdateDto(List<Long> subjectIds) {
}
