package tdenchyk.schooldiaryapp.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tdenchyk.schooldiaryapp.model.Class;

@Repository
public interface ClassRepository extends JpaRepository<Class, Long> {
    @EntityGraph(attributePaths = "teachers")
    Optional<Class> findClassById(final Long id);

    @EntityGraph(attributePaths = "subjects")
    List<Class> findAll();
}
