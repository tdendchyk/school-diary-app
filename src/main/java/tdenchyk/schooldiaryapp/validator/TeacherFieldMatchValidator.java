package tdenchyk.schooldiaryapp.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.validator.annotation.PasswordMatch;

public class TeacherFieldMatchValidator implements
        ConstraintValidator<PasswordMatch, StudentRegisterRequestDto> {
    @Override
    public boolean isValid(StudentRegisterRequestDto requestDto, ConstraintValidatorContext context) {
        String firstField = requestDto.password();
        String secondField = requestDto.repeatPassword();
        return firstField != null && firstField.equals(secondField);
    }
}
