package tdenchyk.schooldiaryapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tdenchyk.schooldiaryapp.model.HomeTask;

@Repository
public interface HomeTaskRepository extends JpaRepository<HomeTask, Long> {
}
