package tdenchyk.schooldiaryapp.controller.rest;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.shaded.org.apache.commons.lang3.builder.EqualsBuilder;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HomeTaskControllerTest {

    protected static MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeAll
    static void beforeAll(@Autowired WebApplicationContext applicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser(value = "teacher", roles = {"TEACHER"})
    @Sql(
            scripts = {"classpath:/database/subjects/add-three-default-subjects.sql",
                    "classpath:/database/class/add-one-class.sql",
                    "classpath:/database/class/add-class-subject-relation.sql",
                    "classpath:/database/schedule/add-one-schedule.sql",
                    "classpath:/database/hometask/add-one-hometask.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/subjects/delete-all-subjects.sql",
                    "classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/hometask/delete-all-hometask.sql",
                    "classpath:/database/schedule/delete-all-schedule.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Get hometask by id")
    public void getHomeTaskById_ValidId_Success() throws Exception {
        HomeTaskResponseDto expected = new HomeTaskResponseDto(1L, "Read 1st paragraph",
                LocalDateTime.of(2024, 2, 28, 9, 30), 1L);

        MvcResult result = mockMvc.perform(get("/api/hometask/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        HomeTaskResponseDto actual = objectMapper.readValue(
                result.getResponse().getContentAsString(), HomeTaskResponseDto.class);

        Assertions.assertNotNull(actual);
        EqualsBuilder.reflectionEquals(expected, actual, "id");
    }
}
