package tdenchyk.schooldiaryapp.dto.student;

public record StudentRegisterResponseDto(Long id,
                                         String firstName,
                                         String lastName,
                                         String email,
                                         String phoneNumber,
                                         Long classNumberId) {
}
