package tdenchyk.schooldiaryapp.service;

import java.util.List;
import tdenchyk.schooldiaryapp.dto.subject.SubjectRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseWithTeacher;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.User;

public interface SubjectService {
    Subject findSubjectById(final Long id);

    List<SubjectResponseDto> findAllSubjects();

    List<SubjectResponseDto> findAllByClassId(final User user);

    SubjectResponseDto addNewSubject(final SubjectRequestDto requestDto);

    void deleteSubject(final Long id);

    List<SubjectResponseWithTeacher> subjectWithTeacherNameBySubjectId(final User user);

    List<SubjectResponseWithTeacher> allSubjectsWithTeacherName();
}
