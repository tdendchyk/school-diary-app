package tdenchyk.schooldiaryapp.service.impl;

import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.ClassSubjectsUpdateDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.ClassMapper;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.repository.ClassRepository;
import tdenchyk.schooldiaryapp.repository.SubjectRepository;
import tdenchyk.schooldiaryapp.service.ClassService;

@Service
@Log4j2
@Transactional
@RequiredArgsConstructor
public class ClassServiceImpl implements ClassService {
    private final ClassRepository classRepository;
    private final SubjectRepository subjectRepository;
    private final ClassMapper classMapper;

    @Override
    public Class findClassById(final Long id) {
        Class foundClass;
        try {
            foundClass = classRepository.findClassById(id)
                    .orElseThrow(
                            () -> new EntityNotFoundException(
                                    "Class with id %d doesn't exist".formatted(id))
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find class. Error: {}", e.getMessage());
            throw e;
        }
        return foundClass;
    }

    @Override
    public List<ClassResponseDto> findAllClasses() {
        return classRepository.findAll().stream()
                .map(classMapper::toDto)
                .toList();
    }

    @Override
    public ClassResponseDto addNewClass(final CreateClassRequestDto requestDto) {
        Class createdClass = classMapper.toClass(requestDto);
        List<Subject> subjects = findSubjects(requestDto.subjectsId());
        subjects
                .forEach(createdClass::addSubject);
        subjects.stream()
                .filter(s -> s.getTeacher() != null)
                .map(Subject::getTeacher)
                .forEach(createdClass::addTeacher);
        Class saved = classRepository.save(createdClass);
        log.info("Class {} saved to DB", requestDto.number());
        return classMapper.toDto(saved);
    }

    @Override
    public ClassResponseDto updateClassSubjects(final Long id,
                                                final ClassSubjectsUpdateDto updateDto) {
        Class foundClass;
        try {
            foundClass = classRepository.findClassById(id).orElseThrow(
                    () -> new EntityNotFoundException(
                            "Class with id %d doesn't exist.".formatted(id))
            );

        } catch (EntityNotFoundException e) {
            log.error("Failed to find class. Error: {}", e.getMessage());
            throw e;
        }
        foundClass.getSubjects().clear();
        deleteClassFromTeachers(foundClass.getTeachers(), foundClass.getId());
        List<Subject> subjects = findSubjects(updateDto.subjectIds());
        subjects.forEach(foundClass::addSubject);
        foundClass.getTeachers().clear();
        findAndAddTeachers(subjects, foundClass);
        return classMapper.toDto(foundClass);
    }

    public void findAndAddTeachers(List<Subject> subjects, Class foundClass) {
        List<Teacher> teachers = subjects.stream()
                .map(Subject::getTeacher)
                .toList();
        teachers.forEach(t -> foundClass.getTeachers().add(t));
    }

    public void deleteClassFromTeachers(final List<Teacher> teachers, final Long classId) {
        for (Teacher teacher : teachers) {
            List<Class> newTeachersClasses = teacher.getClasses().stream()
                    .filter(c -> !c.getId().equals(classId))
                    .toList();
            teacher.setClasses(newTeachersClasses);
        }
    }

    public List<Subject> findSubjects(final List<Long> subjectsId) {
        List<Subject> newSubjects = new ArrayList<>();
        for (Long id : subjectsId) {
            Subject subject;
            try {
                subject = subjectRepository.findById(id).orElseThrow(
                        () -> new EntityNotFoundException(
                                "Subject with id %d doesn't exist.".formatted(id))
                );
            } catch (EntityNotFoundException e) {
                log.error("Failed to find subject. Error: {}", e.getMessage());
                throw e;
            }
            newSubjects.add(subject);
        }
        return newSubjects;
    }
}
