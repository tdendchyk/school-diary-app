package tdenchyk.schooldiaryapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.user.UserResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserUpdateRequestDto;
import tdenchyk.schooldiaryapp.model.User;

@Mapper(config = MapperConfig.class)
public interface UserMapper {
    void updateUser(final UserUpdateRequestDto updateRequestDto, @MappingTarget User user);

    UserResponseDto toResponseDto(final User user);
}
