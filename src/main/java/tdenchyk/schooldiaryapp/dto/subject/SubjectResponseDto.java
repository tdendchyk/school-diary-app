package tdenchyk.schooldiaryapp.dto.subject;

public record SubjectResponseDto(Long id,
                                 String subjectName) {
}
