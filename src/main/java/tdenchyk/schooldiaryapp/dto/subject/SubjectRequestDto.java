package tdenchyk.schooldiaryapp.dto.subject;

import jakarta.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public record SubjectRequestDto(@NotBlank(message = "It should contain at least 1 letter")
                                @Length(max = 255, message = "Should be no longer than 255 letters")
                                String subjectName) {
}
