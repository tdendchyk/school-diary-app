package tdenchyk.schooldiaryapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolDiaryAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchoolDiaryAppApplication.class, args);
    }
}
