package tdenchyk.schooldiaryapp.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public record UserLoginRequestDto(@NotBlank(message = "You can't leave it empty")
                                  @Email(message = "Email should contain @ symbol")
                                  @Length(max = 255, message = "Email should contain less than 255 symbols")
                                  String email,
                                  @NotBlank(message = "You can't leave it empty")
                                  @Length(min = 7, max = 50, message = "Password should be from 7 to 50 symbols long")
                                  String password) {
}
