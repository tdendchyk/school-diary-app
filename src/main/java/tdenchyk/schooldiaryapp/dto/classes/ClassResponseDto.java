package tdenchyk.schooldiaryapp.dto.classes;

import java.util.List;

public record ClassResponseDto(Long id,
                               String number,
                               List<Long> subjectsId) {
}
