package tdenchyk.schooldiaryapp.service;

import java.util.List;
import tdenchyk.schooldiaryapp.dto.schedule.CreateScheduleDto;
import tdenchyk.schooldiaryapp.dto.schedule.GetScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleRequestDto;
import tdenchyk.schooldiaryapp.dto.schedule.ScheduleResponseDto;
import tdenchyk.schooldiaryapp.dto.schedule.TeacherScheduleRequestDto;
import tdenchyk.schooldiaryapp.model.Schedule;

public interface ScheduleService {
    Schedule findById(Long id);

    List<GetScheduleResponseDto> getStudentsSchedule(final ScheduleRequestDto requestDto);

    List<GetScheduleResponseDto> getTeachersSchedule(final TeacherScheduleRequestDto requestDto);

    List<ScheduleResponseDto> createStudentsSchedule(
            final List<CreateScheduleDto> scheduleDtos);

    ScheduleResponseDto createStudentsSchedule(
            final CreateScheduleDto scheduleDtos);

    void deleteScheduleById(final Long id);
}
