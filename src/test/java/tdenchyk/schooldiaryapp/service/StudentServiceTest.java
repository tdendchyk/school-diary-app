package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterResponseDto;
import tdenchyk.schooldiaryapp.exception.RegistrationException;
import tdenchyk.schooldiaryapp.mapper.StudentMapper;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Role;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.repository.RoleRepository;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.impl.StudentServiceImpl;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {
    private static final Student student;
    private static final StudentRegisterRequestDto registerRequestDto;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private StudentMapper studentMapper;
    @Spy
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @InjectMocks
    private StudentServiceImpl studentService;

    static {
        Class classNumber = new Class();
        classNumber.setId(1L);

        student = new Student();
        student.setId(1L);
        student.setFirstName("Maria");
        student.setLastName("Rudenko");
        student.setEmail("rudenko.mar@gmail.com");
        student.setPassword("password");
        student.setPhoneNumber("380997998623");
        student.setClassNumberId(classNumber);

        registerRequestDto = new StudentRegisterRequestDto("Maria",
                "Rudenko", "rudenko.mar@gmail.com",
                "password", "password", "380997998623", 1L);
    }

    @Test
    @DisplayName("Register student")
    public void register_ValidStudent_ReturnsStudentRegisterResponseDto() {
        Role studentRole = new Role();
        studentRole.setName(Role.RoleName.STUDENT);
        StudentRegisterResponseDto responseDto = new StudentRegisterResponseDto(1L, "Maria",
                "Rudenko", "rudenko.mar@gmail.com", "380997998623", 1L);

        when(userRepository.existsByEmail(any(String.class))).thenReturn(false);
        when(roleRepository.findRoleByName(any(Role.RoleName.class))).thenReturn(studentRole);
        when(studentMapper.toStudent(registerRequestDto)).thenReturn(student);
        when(userRepository.save(student)).thenReturn(student);
        when(studentMapper.toDto(student)).thenReturn(responseDto);

        StudentRegisterResponseDto actual = studentService.register(registerRequestDto);

        assertThat(student.getRoles()).contains(studentRole);
        assertThat(actual.classNumberId()).isEqualTo(1L);
        assertThat(actual.email()).isEqualTo("rudenko.mar@gmail.com");
    }

    @Test
    @DisplayName("Fail on registration of existent user")
    public void register_ExistentUser_ThrowsRegistrationException() {
        when(userRepository.existsByEmail(any(String.class))).thenReturn(true);

        assertThrows(RegistrationException.class,
                () -> studentService.register(registerRequestDto));
    }
}
