package tdenchyk.schooldiaryapp.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tdenchyk.schooldiaryapp.dto.student.StudentResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserUpdateRequestDto;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.service.UserService;

@RestController
@Log4j2
@RequiredArgsConstructor
@Tag(name = "User Controller",
        description = """
                Endpoints to get info about students and teachers,
                delete users or update info on them""")
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Delete a student",
            description = "Delete a student by id")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/student/delete/{id}")
    public void deleteStudent(@PathVariable final Long id) {
        log.info("Deleting student with id {}", id);
        userService.deleteStudentById(id);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Delete a teacher",
            description = "Delete a teacher by id")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/teacher/delete/{id}")
    public void deleteTeacher(@PathVariable final Long id) {
        log.info("Deleting teacher with id {}", id);
        userService.deleteTeacherById(id);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Update info on user",
            description = "Update firstName, lastName, email, password or phoneNumber")
    @PutMapping("/update/{userId}")
    public UserResponseDto updateInfoOnUser(
            @RequestBody @Valid final UserUpdateRequestDto updateRequestDto,
            final Authentication authentication,
            @PathVariable final Long userId) {
        final User user = (User) authentication.getPrincipal();
        log.info("Updating info on user with id {}", user.getId());
        return userService.updateUser(user, updateRequestDto, userId);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get all students by class id")
    @GetMapping("/classes/{classId}/students")
    public List<StudentResponseDto> getAllStudentsByClassId(@PathVariable final Long classId) {
        log.info("Getting students from class with id {}", classId);
        return userService.findAllStudentsByClassId(classId);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get all teachers by class id")
    @GetMapping("/classes/{classId}/teachers")
    public List<TeacherResponseDto> getAllTeachersByClassId(@PathVariable final Long classId) {
        log.info("Retrieving teachers by class id {}", classId);
        return userService.findAllTeachersByClassId(classId);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get a student by id")
    @GetMapping("/students/{id}")
    public StudentResponseDto getStudentById(@PathVariable final Long id) {
        return userService.findStudentById(id);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get a teacher by id")
    @GetMapping("/teachers/{id}")
    public TeacherResponseDto getTeacherById(@PathVariable final Long id) {
        return userService.findTeacherById(id);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get all students")
    @GetMapping("/students/all")
    public List<StudentResponseDto> getAllStudents() {
        return userService.findAllStudents();
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Get all teachers")
    @GetMapping("/teachers/all")
    public List<TeacherResponseDto> getAllTeachers() {
        return userService.findAllTeachers();
    }
}
