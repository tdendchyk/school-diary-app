package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tdenchyk.schooldiaryapp.dto.student.StudentResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserUpdateRequestDto;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.StudentMapper;
import tdenchyk.schooldiaryapp.mapper.TeacherMapper;
import tdenchyk.schooldiaryapp.mapper.UserMapper;
import tdenchyk.schooldiaryapp.mapper.impl.UserMapperImpl;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Role;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.StudentRepository;
import tdenchyk.schooldiaryapp.repository.TeacherRepository;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    private static final Student student1;
    private static final Student student2;

    private static final StudentResponseDto studentResponseDto1;
    private static final StudentResponseDto studentResponseDto2;

    private static final Teacher teacher1;
    private static final Teacher teacher2;
    private static final TeacherResponseDto teacherResponseDto1;
    private static final TeacherResponseDto teacherResponseDto2;
    @Mock
    private UserRepository userRepository;
    @Mock
    private StudentRepository studentRepository;
    @Mock
    private TeacherRepository teacherRepository;
    @Mock
    private TeacherMapper teacherMapper;
    @Mock
    private StudentMapper studentMapper;
    @Spy
    private UserMapper userMapper = new UserMapperImpl();
    @Spy
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Mock
    private ClassService classService;

    @InjectMocks
    private UserServiceImpl userService;

    static {
        student1 = new Student();
        student1.setId(1L);
        student1.setFirstName("Max");
        student1.setLastName("Cherkashyn");
        student1.setEmail("mCherkashyn@gmail.com");
        student1.setClassNumberId(new Class());

        student2 = new Student();
        student2.setId(2L);
        student2.setFirstName("Olena");
        student2.setLastName("Shevchenko");
        student2.setEmail("oShevchenko@gmail.com");
        student2.setClassNumberId(new Class());

        studentResponseDto1 = new StudentResponseDto(1L, "Max",
                "Cherkashyn", "380975641980");
        studentResponseDto2 = new StudentResponseDto(2L, "Olena",
                "Shevchenko", "380742983224");

        teacher1 = new Teacher();
        teacher1.setId(3L);
        teacher1.setFirstName("Maryna");
        teacher1.setLastName("Matyah");
        teacher1.setEmail("mMatyah@gmail.com");
        teacher1.setPhoneNumber("380907867567");

        teacher2 = new Teacher();
        teacher2.setId(4L);
        teacher2.setFirstName("Nina");
        teacher2.setLastName("Cherevko");
        teacher2.setEmail("nCherevko@gmail.com");
        teacher2.setPhoneNumber("380907867567");

        teacherResponseDto1 = new TeacherResponseDto(3L, "Maryna",
                "Matyah", "380907867567", null);
        teacherResponseDto2 = new TeacherResponseDto(4L, "Nina",
                "Cherevko", "380907867567", null);
    }

    @Test
    @DisplayName("Find all students")
    public void findAllStudents_ReturnsListOfStudentResponseDto() {
        List<Student> students = List.of(student1, student2);

        when(studentRepository.findAll()).thenReturn(students);
        when(studentMapper.toResponseDto(student1)).thenReturn(studentResponseDto1);
        when(studentMapper.toResponseDto(student2)).thenReturn(studentResponseDto2);

        List<StudentResponseDto> actual = userService.findAllStudents();

        assertThat(actual.get(0).firstName()).isEqualTo("Max");
        assertThat(actual.get(1).lastName()).isEqualTo("Shevchenko");
        assertThat(actual).hasSize(2);
    }

    @Test
    @DisplayName("Find all teachers")
    public void findAllTeachers_ReturnsListOfTeacherResponseDto() {
        List<Teacher> teachers = List.of(teacher1, teacher2);

        when(teacherRepository.findAll()).thenReturn(teachers);
        when(teacherMapper.toResponseDto(teacher1)).thenReturn(teacherResponseDto1);
        when(teacherMapper.toResponseDto(teacher2)).thenReturn(teacherResponseDto2);

        List<TeacherResponseDto> actual = userService.findAllTeachers();

        assertThat(actual.get(0).id()).isEqualTo(teacherResponseDto1.id());
        assertThat(actual.get(1).firstName()).isEqualTo(teacherResponseDto2.firstName());
        assertThat(actual).hasSize(2);
    }

    @Test
    @DisplayName("Find student by id")
    public void findStudentById_ValidId_ReturnsStudentResponseDto() {
        when(studentRepository.findById(any(Long.class))).thenReturn(Optional.of(student1));
        when(studentMapper.toResponseDto(student1)).thenReturn(studentResponseDto1);

        StudentResponseDto actual = userService.findStudentById(1L);

        assertThat(actual.firstName()).isEqualTo("Max");
        assertThat(actual.id()).isEqualTo(1L);
    }

    @Test
    @DisplayName("Fail on finding student by invalid id")
    public void findStudentById_InvalidId_ThrowsEntityNotFoundException() {
        when(studentRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.findStudentById(4L));
    }

    @Test
    @DisplayName("Find teacher by id")
    public void findTeacherById_ValidId_ReturnsTeacherResponseDto() {
        when(teacherRepository.findById(any(Long.class))).thenReturn(Optional.of(teacher1));
        when(teacherMapper.toResponseDto(teacher1)).thenReturn(teacherResponseDto1);

        TeacherResponseDto actual = userService.findTeacherById(3L);

        assertThat(actual.firstName()).isEqualTo("Maryna");
        assertThat(actual.phoneNumber()).isEqualTo("380907867567");
    }

    @Test
    @DisplayName("Fail on finding teacher by invalid id")
    public void findTeacherById_InvalidId_ThrowsEntityNotFoundException() {
        when(teacherRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.findTeacherById(1L));
    }

    @Test
    @DisplayName("Find all students by class id")
    public void findAllStudentsByClassId_ValidId_ReturnsListOfStudentResponseDto() {
        when(classService.findClassById(1L)).thenReturn(new Class());
        when(studentRepository.findAllByClassId(1L)).thenReturn(List.of(student1, student2));
        when(studentMapper.toResponseDto(student1)).thenReturn(studentResponseDto1);
        when(studentMapper.toResponseDto(student2)).thenReturn(studentResponseDto2);

        List<StudentResponseDto> actual = userService.findAllStudentsByClassId(1L);

        assertThat(actual).hasSize(2);
        assertThat(actual.get(0).lastName()).isEqualTo("Cherkashyn");
        assertThat(actual.get(1).phoneNumber()).isEqualTo("380742983224");
    }

    @Test
    @DisplayName("Find all teachers by class id")
    public void findAllTeachersByClassId_ValidId_ReturnsListOfTeacherResponseDto() {
        Class mockedClass = spy(Class.class);
        mockedClass.setTeachers(List.of(teacher1, teacher2));
        when(classService.findClassById(1L)).thenReturn(mockedClass);
        when(teacherMapper.toResponseDto(teacher1)).thenReturn(teacherResponseDto1);
        when(teacherMapper.toResponseDto(teacher2)).thenReturn(teacherResponseDto2);

        List<TeacherResponseDto> actual = userService.findAllTeachersByClassId(1L);

        assertThat(actual).hasSize(2);
        assertThat(actual.get(0).lastName()).isEqualTo("Matyah");
        assertThat(actual.get(1).phoneNumber()).isEqualTo("380907867567");
    }

    @Test
    @DisplayName("Delete student by id")
    public void deleteStudentById_ValidId_Ok() {
        User mockedStudent = spy(student1);
        Role role = new Role();
        role.setName(Role.RoleName.STUDENT);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        mockedStudent.setRoles(roles);

        when(userRepository.findById(1L)).thenReturn(Optional.of(mockedStudent));

        assertAll(() -> userService.deleteStudentById(1L));
    }

    @Test
    @DisplayName("Fail on deleting user with role of teacher")
    public void deleteStudentById_InvalidRole_ThrowsEntityNotFoundException() {
        User mockedStudent = spy(student1);
        Role invalidRole = new Role();
        invalidRole.setName(Role.RoleName.TEACHER);
        Set<Role> roles = new HashSet<>();
        roles.add(invalidRole);
        mockedStudent.setRoles(roles);

        when(userRepository.findById(1L)).thenReturn(Optional.of(mockedStudent));

        assertThrows(EntityNotFoundException.class, () -> userService.deleteStudentById(1L));
    }

    @Test
    @DisplayName("Fail on deleting student by invalid id")
    public void deleteStudentById_InvalidId_ThrowsEntityNotFoundException() {
        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.deleteStudentById(1L));
    }

    @Test
    @DisplayName("Delete teacher by id")
    public void deleteTeacherById_ValidId_Ok() {
        User mockedTeacher = spy(teacher1);
        Role role = new Role();
        role.setName(Role.RoleName.TEACHER);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        mockedTeacher.setRoles(roles);

        when(userRepository.findById(3L)).thenReturn(Optional.of(mockedTeacher));

        assertAll(() -> userService.deleteTeacherById(3L));
    }

    @Test
    @DisplayName("Fail on deleting teacher by invalid id")
    public void deleteTeacherById_InvalidId_ThrowsEntityNotFoundException() {
        when(userRepository.findById(5L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.deleteTeacherById(5L));
    }

    @Test
    @DisplayName("Update user's info")
    public void updateUser_ValidInfo_ReturnsUserResponseDto() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("Maria");
        user.setLastName("Bocharnikova");
        user.setEmail("mBocharnikova@gmail.com");
        user.setPassword("password");
        user.setPhoneNumber("380974411432");
        UserUpdateRequestDto requestDto =
                new UserUpdateRequestDto(1L,
                        "Maria",
                        "Bocharnikova",
                        "mBocharnikova@gmail.com",
                        "380974411000");
        user.setPhoneNumber("380974411000");
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        UserResponseDto actual = userService.updateUser(user, requestDto, 1L);

        assertThat(actual.email()).isEqualTo("mBocharnikova@gmail.com");
        assertThat(actual.phoneNumber()).isEqualTo("380974411000");
    }
}
