package tdenchyk.schooldiaryapp.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.UserRepository;

@Service
@Log4j2
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user;
        try {
            user = userRepository.findByEmail(username)
                    .orElseThrow(
                            () -> new EntityNotFoundException("Oops... Such user doesn't exist")
                    );
        } catch (EntityNotFoundException e) {
            log.error("Failed to find user. User with username {} doesn't exist", username);
            throw e;
        }
        return user;
    }
}
