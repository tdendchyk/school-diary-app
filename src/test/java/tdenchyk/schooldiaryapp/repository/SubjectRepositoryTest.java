package tdenchyk.schooldiaryapp.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Subject;

@DataJpaTest
public class SubjectRepositoryTest {
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private ClassRepository classRepository;

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    @DisplayName("Find all subjects by class id")
    public void findAllByClassNameId_ReturnsListOfSubjects() {
        Subject subject1 = new Subject();
        subject1.setSubjectName("Maths");
        Subject subject2 = new Subject();
        subject2.setSubjectName("History");
        Subject subject3 = new Subject();
        subject3.setSubjectName("Literature");
        subjectRepository.saveAll(List.of(subject1, subject2, subject3));
        Class class1 = new Class();
        class1.setNumber("10");
        class1.setSubjects(List.of(subject1, subject2));
        Class class2 = new Class();
        class2.setNumber("11");
        class2.setSubjects(List.of(subject3));
        classRepository.saveAll(List.of(class1, class2));

        List<Subject> actual = subjectRepository.findAllByClassNameId(1L);

        assertThat(actual).hasSize(2);
        assertThat(actual.get(0).getSubjectName()).isEqualTo("Maths");
        assertThat(actual.get(1).getSubjectName()).isEqualTo("History");
    }
}



