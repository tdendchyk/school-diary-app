package tdenchyk.schooldiaryapp.service.impl;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterResponseDto;
import tdenchyk.schooldiaryapp.exception.RegistrationException;
import tdenchyk.schooldiaryapp.mapper.TeacherMapper;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Role;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.repository.RoleRepository;
import tdenchyk.schooldiaryapp.repository.SubjectRepository;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.TeacherService;

@Service
@Log4j2
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final SubjectRepository subjectRepository;
    private final TeacherMapper teacherMapper;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public TeacherRegisterResponseDto register(final TeacherRegisterRequestDto requestDto) {
        if (userRepository.existsByEmail(requestDto.email())) {
            log.error("Failed to register user with email {}, it already exists",
                    requestDto.email());
            throw new RegistrationException("Unable to complete registration. User already exists");
        }
        Role defaultRole = roleRepository.findRoleByName(Role.RoleName.TEACHER);
        Teacher teacher = teacherMapper.toTeacher(requestDto);
        if (teacher.getSubject() != null) {
            addTeacherToClasses(teacher);
        }
        teacher.setRoles(Set.of(defaultRole));
        teacher.setPassword(passwordEncoder.encode(requestDto.password()));
        return teacherMapper.toDto(userRepository.save(teacher));
    }

    @Transactional
    public void addTeacherToClasses(final Teacher teacher) {
        Subject subject = teacher.getSubject();
        List<Class> classes = subject.getClasses();
        classes.forEach(c -> c.getTeachers().add(teacher));
        subjectRepository.save(subject);
        classes.forEach(c -> teacher.getClasses().add(c));
        log.info("Teacher with email {} was added to classes", teacher.getEmail());
    }
}
