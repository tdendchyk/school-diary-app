package tdenchyk.schooldiaryapp.service;

import java.util.List;
import tdenchyk.schooldiaryapp.dto.student.StudentResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserUpdateRequestDto;
import tdenchyk.schooldiaryapp.model.User;

public interface UserService {
    List<StudentResponseDto> findAllStudents();

    List<TeacherResponseDto> findAllTeachers();

    StudentResponseDto findStudentById(final Long id);

    TeacherResponseDto findTeacherById(final Long id);

    List<StudentResponseDto> findAllStudentsByClassId(final Long id);

    List<TeacherResponseDto> findAllTeachersByClassId(final Long id);

    UserResponseDto updateUser(final User user,
                               final UserUpdateRequestDto requestDto,
                               final Long userId);

    UserResponseDto updateUser(final User user,
                               final UserUpdateRequestDto requestDto);

    void deleteStudentById(final Long id);

    void deleteTeacherById(final Long id);
}
