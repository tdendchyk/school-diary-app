package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterResponseDto;
import tdenchyk.schooldiaryapp.exception.RegistrationException;
import tdenchyk.schooldiaryapp.mapper.TeacherMapper;
import tdenchyk.schooldiaryapp.model.Role;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.repository.RoleRepository;
import tdenchyk.schooldiaryapp.repository.SubjectRepository;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.impl.TeacherServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TeacherServiceTest {
    private static final Teacher teacher;
    private static final TeacherRegisterRequestDto registerRequestDto;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private SubjectRepository subjectRepository;
    @Mock
    private TeacherMapper teacherMapper;
    @Spy
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @InjectMocks
    private TeacherServiceImpl teacherService;

    static {
        teacher = new Teacher();
        teacher.setId(1L);
        teacher.setFirstName("Maria");
        teacher.setLastName("Rudenko");
        teacher.setEmail("rudenko.mar@gmail.com");
        teacher.setPassword("password");
        teacher.setPhoneNumber("380997998623");
        teacher.setSubject(new Subject());

        registerRequestDto = new TeacherRegisterRequestDto("Maria",
                "Rudenko", "rudenko.mar@gmail.com",
                "password","password", "380997998623", 1L);
    }

    @Test
    @DisplayName("Register a teacher")
    public void register_ValidTeacher_ReturnsTeacherRegisterResponseDto() {
        Role teacherRole = new Role();
        teacherRole.setName(Role.RoleName.TEACHER);
        TeacherRegisterResponseDto responseDto = new TeacherRegisterResponseDto(1L, "Maria",
                "Rudenko", "rudenko.mar@gmail.com", "380997998623", 1L);

        when(userRepository.existsByEmail(any(String.class))).thenReturn(false);
        when(roleRepository.findRoleByName(any(Role.RoleName.class))).thenReturn(teacherRole);
        when(teacherMapper.toTeacher(registerRequestDto)).thenReturn(teacher);
        when(userRepository.save(teacher)).thenReturn(teacher);
        when(teacherMapper.toDto(teacher)).thenReturn(responseDto);

        TeacherRegisterResponseDto actual = teacherService.register(registerRequestDto);

        assertThat(teacher.getRoles()).contains(teacherRole);
        assertThat(actual.subjectId()).isEqualTo(1L);
        assertThat(actual.email()).isEqualTo("rudenko.mar@gmail.com");
    }

    @Test
    @DisplayName("Fail on registration of existent user")
    public void register_ExistentUser_ThrowsRegistrationException() {
        when(userRepository.existsByEmail(any(String.class))).thenReturn(true);

        assertThrows(RegistrationException.class,
                () -> teacherService.register(registerRequestDto));
    }
}
