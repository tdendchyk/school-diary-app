package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import tdenchyk.schooldiaryapp.dto.subject.SubjectRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.SubjectMapper;
import tdenchyk.schooldiaryapp.mapper.impl.SubjectMapperImpl;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.repository.ScheduleRepository;
import tdenchyk.schooldiaryapp.repository.SubjectRepository;
import tdenchyk.schooldiaryapp.service.impl.SubjectServiceImpl;

@ExtendWith(MockitoExtension.class)
public class SubjectServiceTest {
    private static final Subject subject1;
    private static final Subject subject2;
    @Mock
    private SubjectRepository subjectRepository;

    @Mock
    private ScheduleRepository scheduleRepository;

    @Spy
    private SubjectMapper subjectMapper = new SubjectMapperImpl();
    @InjectMocks
    private SubjectServiceImpl subjectService;

    static {
        subject1 = new Subject();
        subject1.setId(1L);
        subject1.setSubjectName("Math");

        subject2 = new Subject();
        subject2.setId(2L);
        subject2.setSubjectName("History");
    }

    @Test
    @DisplayName("Find subject by id")
    public void findSubjectById_ValidSubject_ReturnsSubject() {
        when(subjectRepository.findById(any(Long.class))).thenReturn(Optional.of(subject1));

        Subject actual = subjectService.findSubjectById(1L);

        assertThat(actual.getId()).isEqualTo(1L);
        assertThat(actual.getSubjectName()).isEqualTo(subject1.getSubjectName());
    }

    @Test
    @DisplayName("Find subject by invalid id")
    public void findSubjectById_InvalidId_ThrowsEntityNotFoundException() {
        when(subjectRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> subjectService.findSubjectById(1L));
    }

    @Test
    @DisplayName("Find all subjects")
    public void findAllSubjects_ReturnsListOfSubjectResponseDto() {
        List<Subject> subjects = List.of(subject1, subject2);

        when(subjectRepository.findAll()).thenReturn(subjects);

        List<SubjectResponseDto> actual = subjectService.findAllSubjects();

        assertThat(actual).hasSize(2);
        assertThat(actual.get(0).subjectName()).isEqualTo("Math");
        assertThat(actual.get(1).subjectName()).isEqualTo("History");
    }

    @Test
    @DisplayName("Find all subjects by their classId")
    public void findAllByClassId_ClassesWithValidClassId_ReturnsListOfSubjectResponseDto() {
        Student mockStudent = spy(Student.class);

        Class ourClass = new Class();
        ourClass.setId(1L);
        Class mockClass = spy(ourClass);

        List<Subject> subjects = List.of(subject1, subject2);

        when(mockStudent.getClassNumberId()).thenReturn(mockClass);
        when(mockClass.getId()).thenReturn(1L);
        when(subjectRepository.findAllByClassNameId(any(Long.class))).thenReturn(subjects);

        List<SubjectResponseDto> actual = subjectService.findAllByClassId(mockStudent);

        assertThat(actual).hasSize(2);
        assertThat(actual.get(0).subjectName()).isEqualTo("Math");
        assertThat(actual.get(1).subjectName()).isEqualTo("History");
    }

    @Test
    @DisplayName("Add new subject")
    public void addNewSubject_ValidSubject_ReturnsSubjectResponseDto() {
        SubjectRequestDto requestDto = new SubjectRequestDto("Math");
        when(subjectMapper.toSubject(any(SubjectRequestDto.class))).thenReturn(subject1);
        when(subjectRepository.save(any(Subject.class))).thenReturn(subject1);

        SubjectResponseDto actual = subjectService.addNewSubject(requestDto);

        assertThat(actual.subjectName()).isEqualTo(subject1.getSubjectName());
        assertThat(actual.id()).isEqualTo(subject1.getId());
    }

    @Test
    @DisplayName("Delete subject by id")
    public void deleteSubjectById_ValidId_Ok() {
        when(subjectRepository.findById(any(Long.class))).thenReturn(Optional.of(subject1));
        when(scheduleRepository.findAll()).thenReturn(List.of());
        assertAll(() -> subjectService.deleteSubject(1L));
    }

    @Test
    @DisplayName("Delete subject by invalid id")
    public void deleteSubjectById_InvalidId_ThrowsEntityNotFoundException() {
        when(subjectRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> subjectService.deleteSubject(1L));
    }
}
