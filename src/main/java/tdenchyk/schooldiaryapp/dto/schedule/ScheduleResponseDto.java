package tdenchyk.schooldiaryapp.dto.schedule;

import java.time.LocalTime;
import tdenchyk.schooldiaryapp.model.Schedule;

public record ScheduleResponseDto(Long id,
                                  Long classId,
                                  Schedule.Day day,
                                  Long subjectId,
                                  LocalTime startTime,
                                  LocalTime endTime) {
}
