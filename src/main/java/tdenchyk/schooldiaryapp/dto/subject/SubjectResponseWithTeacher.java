package tdenchyk.schooldiaryapp.dto.subject;

public record SubjectResponseWithTeacher(Long id,
                                         String subjectName,
                                         String teacherName) {
}
