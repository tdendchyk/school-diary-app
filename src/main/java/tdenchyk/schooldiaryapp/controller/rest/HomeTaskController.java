package tdenchyk.schooldiaryapp.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.service.HomeTaskService;

@RestController
@RequiredArgsConstructor
@Log4j2
@Tag(name = "HomeTask Controller", description = "Endpoints to browse and modify hometasks")
@RequestMapping("api/hometask")
public class HomeTaskController {
    private final HomeTaskService homeTaskService;

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Add a new hometask for a certain subject")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/add")
    public HomeTaskResponseDto addNewHomeTask(
            @RequestBody @Valid final HomeTaskRequestDto requestDto,
            final Authentication authentication) {
        final User user = (User) authentication.getPrincipal();
        log.info("Adding new hometask by user with email {}", user.getEmail());
        return homeTaskService.addHomeTask(user, requestDto);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Update an existent task for hometask for a certain subject")
    @PatchMapping("/update/{id}")
    public HomeTaskResponseDto updateHomeTask(@PathVariable final Long id,
                               @RequestBody @Valid final HomeTaskRequestDto requestDto,
                               final Authentication authentication) {
        final User user = (User) authentication.getPrincipal();
        log.info("User with id {} updates hometask with id {}", user.getId(), id);
        return homeTaskService.updateHomeTask(id, user, requestDto);
    }

    @PreAuthorize("hasRole('TEACHER')")
    @Operation(summary = "Delete an existent hometask from DB")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete/{id}")
    public void deleteHomeTask(@PathVariable final Long id) {
        log.info("Deleting hometask with id {}", id);
        homeTaskService.deleteById(id);
    }

    @PreAuthorize("hasAnyRole('STUDENT', 'TEACHER')")
    @Operation(summary = "Retrieve a hometask by id")
    @GetMapping("/{id}")
    public HomeTaskResponseDto getHomeTaskById(@PathVariable final Long id) {
        return homeTaskService.findById(id);
    }
}
