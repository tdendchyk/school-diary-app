package tdenchyk.schooldiaryapp.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public record UserUpdateRequestDto(Long id,
                                   @NotBlank(message = "First name should contain at least 1 letter")
                                   @Length(max = 200, message = "First name should contain less than 200 symbols")
                                   String firstName,
                                   @NotBlank(message = "Last name should contain at least 1 letter")
                                   @Length(max = 200, message = "Last name should contain less than 200 symbols")
                                   String lastName,
                                   @NotBlank(message = "You can't leave it empty")
                                   @Email(message = "Email should contain @ symbol")
                                   @Length(max = 255, message = "Email should contain less than 255 symbols")
                                   String email,
                                   @NotBlank(message = "You can't leave it empty")
                                   String phoneNumber) {
}
