package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tdenchyk.schooldiaryapp.service.ClassService;

@Controller
@RequiredArgsConstructor
@Tag(name = "MVC Start Controller",
        description = "Views with home page and page with roles")
@RequestMapping("/start")
public class StartController {
    private final ClassService classService;

    @Operation(summary = "Shows necessary information about the app")
    @GetMapping
    public String aboutPage() {
        return "start/about";
    }

    @Operation(summary = "Page to choose if user is a teacher or a student",
                description = "First step to register a new user")
    @GetMapping("/home")
    public String chooseRolePage(Model model) {
        model.addAttribute("classes", classService.findAllClasses());
        return "start/your-role";
    }
}
