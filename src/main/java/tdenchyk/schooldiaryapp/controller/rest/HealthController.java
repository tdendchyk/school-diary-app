package tdenchyk.schooldiaryapp.controller.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/health")
public class HealthController {
    @GetMapping
    public String doCheck(Model model) {
        model.addAttribute("appName", "School Diary");
        return "check";
    }
}
