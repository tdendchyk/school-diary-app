package tdenchyk.schooldiaryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.ClassSubjectsUpdateDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.mapper.ClassMapper;
import tdenchyk.schooldiaryapp.mapper.impl.ClassMapperImpl;
import tdenchyk.schooldiaryapp.model.Class;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.model.Teacher;
import tdenchyk.schooldiaryapp.repository.ClassRepository;
import tdenchyk.schooldiaryapp.repository.SubjectRepository;
import tdenchyk.schooldiaryapp.service.impl.ClassServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ClassServiceTest {
    private static final Subject subject1;
    private static final Subject subject2;
    private static final Class foundClass;
    private static final List<Subject> subjects;
    @Mock
    private ClassRepository classRepository;
    @Mock
    private SubjectRepository subjectRepository;
    @Spy
    private ClassMapper classMapper = new ClassMapperImpl();
    @InjectMocks
    private ClassServiceImpl classService;

    static {
        subject1 = new Subject();
        subject1.setId(1L);
        subject1.setSubjectName("Math");
        subject1.setTeacher(new Teacher());
        subject2 = new Subject();
        subject2.setId(2L);
        subject2.setSubjectName("History");
        subject2.setTeacher(new Teacher());

        subjects = new ArrayList<>();
        subjects.add(subject1);
        subjects.add(subject2);

        foundClass = new Class();
        foundClass.setId(1L);
        foundClass.setNumber("9");
        foundClass.setSubjects(subjects);
    }

    @Test
    @DisplayName("Find class by valid id")
    public void findClassById_ValidId_ReturnsClass() {
        when(classRepository.findClassById(1L)).thenReturn(Optional.of(foundClass));

        Class actual = classService.findClassById(1L);

        assertThat(actual.getId()).isEqualTo(1L);
        assertThat(actual.getSubjects()).hasSize(2);
        assertThat(actual.getNumber()).isEqualTo("9");
    }

    @Test
    @DisplayName("Fail to find class by invalid id")
    public void findClassById_InvalidId_ThrowsEntityNotFoundException() {
        when(classRepository.findClassById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> classService.findClassById(1L));
    }

    @Test
    @DisplayName("Add new class with subjects")
    public void addNewClass_ValidClass_ReturnsClassResponseDto() {
        Subject subject1 = new Subject();
        subject1.setId(1L);
        subject1.setSubjectName("Math");
        subject1.setTeacher(new Teacher());
        Subject subject2 = new Subject();
        subject2.setId(2L);
        subject2.setSubjectName("History");
        subject2.setTeacher(new Teacher());

        List<Subject> subjects = new ArrayList<>();
        subjects.add(subject1);
        subjects.add(subject2);

        Class newClass = new Class();
        newClass.setId(1L);
        newClass.setNumber("9");
        newClass.setSubjects(subjects);
        List<Long> subjectIds = new ArrayList<>();
        subjectIds.add(1L);
        subjectIds.add(2L);

        CreateClassRequestDto requestDto = new CreateClassRequestDto("9", subjectIds);
        ClassResponseDto responseDto = new ClassResponseDto(1L, "9", subjectIds);

        when(classMapper.toClass(requestDto)).thenReturn(newClass);
        when(classRepository.save(any(Class.class))).thenReturn(newClass);
        when(subjectRepository.findById(1L)).thenReturn(Optional.of(subject1));
        when(subjectRepository.findById(2L)).thenReturn(Optional.of(subject2));
        when(classMapper.toDto(newClass)).thenReturn(responseDto);

        ClassResponseDto actual = classService.addNewClass(requestDto);

        assertThat(actual.number()).isEqualTo("9");
        assertThat(actual.subjectsId().get(0)).isEqualTo(1L);
        assertThat(actual.subjectsId().get(1)).isEqualTo(2L);
    }

    @Test
    @DisplayName("Update subjects of class")
    public void updateClassSubjects_ValidSubjects_ReturnsClassResponseDto() {
        Class newClass = new Class();
        newClass.setId(2L);
        newClass.setNumber("10");
        Subject subject1 = new Subject();
        subject1.setId(3L);
        subject1.setTeacher(new Teacher());
        Subject subject2 = new Subject();
        subject2.setId(4L);
        subject2.setTeacher(new Teacher());
        List<Subject> subjects = new ArrayList<>();
        subjects.add(subject1);
        subjects.add(subject2);
        newClass.setSubjects(subjects);
        List<Long> subjectIds = new ArrayList<>();
        subjectIds.add(3L);
        subjectIds.add(4L);
        ClassSubjectsUpdateDto updateDto = new ClassSubjectsUpdateDto(subjectIds);
        ClassResponseDto responseDto = new ClassResponseDto(2L, "10", subjectIds);

        when(classRepository.findClassById(any(Long.class))).thenReturn(Optional.of(newClass));
        when(subjectRepository.findById(3L)).thenReturn(Optional.of(subject1));
        when(subjectRepository.findById(4L)).thenReturn(Optional.of(subject2));
        when(classMapper.toDto(newClass)).thenReturn(responseDto);

        ClassResponseDto actual = classService.updateClassSubjects(2L, updateDto);

        assertThat(actual.subjectsId().get(0)).isEqualTo(3L);
        assertThat(actual.subjectsId().get(1)).isEqualTo(4L);
    }

    @Test
    @DisplayName("Fail on updating subjects of invalid class")
    public void updateClassSubjects_InvalidClass_ThrowsEntityNotFoundException() {
        List<Long> subjectsIds = new ArrayList<>();
        subjectsIds.add(1L);
        subjectsIds.add(2L);
        ClassSubjectsUpdateDto updateDto = new ClassSubjectsUpdateDto(subjectsIds);

        when(classRepository.findClassById(any(Long.class))).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> classService.updateClassSubjects(1L, updateDto));
    }

    @Test
    @DisplayName(value = "Find all classes")
    public void findAllClasses_Valid_ShouldReturnAllClasses() {
        List<Class> classes = Collections.singletonList(foundClass);

        when(classRepository.findAll()).thenReturn(classes);

        List<ClassResponseDto> allClasses = classService.findAllClasses();

        assertThat(allClasses).hasSize(1);
        assertThat(allClasses.get(0).number()).isEqualTo("9");
    }
}
