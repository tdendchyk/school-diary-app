package tdenchyk.schooldiaryapp.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterResponseDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterResponseDto;
import tdenchyk.schooldiaryapp.dto.user.UserLoginRequestDto;
import tdenchyk.schooldiaryapp.dto.user.UserLoginResponseDto;
import tdenchyk.schooldiaryapp.security.AuthenticationService;
import tdenchyk.schooldiaryapp.service.StudentService;
import tdenchyk.schooldiaryapp.service.TeacherService;

@RestController
@RequiredArgsConstructor
@Log4j2
@RequestMapping("/api/auth")
@Tag(name = "Authentication controller",
        description = "Endpoints for login and registration for students and teachers")
public class AuthController {
    private final StudentService studentService;
    private final TeacherService teacherService;
    private final AuthenticationService authenticationService;

    @Operation(summary = "Register",
            description = "Register new student")
    @PostMapping("/register/student")
    @ResponseStatus(HttpStatus.CREATED)
    public StudentRegisterResponseDto registerStudent(
            @RequestBody @Valid final StudentRegisterRequestDto request) {
        log.info("User with email {} registers as student", request.email());
        return studentService.register(request);
    }

    @Operation(summary = "Register",
            description = "Register new teacher")
    @PostMapping("/register/teacher")
    @ResponseStatus(HttpStatus.CREATED)
    public TeacherRegisterResponseDto registerTeacher(
            @RequestBody @Valid final TeacherRegisterRequestDto requestDto) {
        log.info("User with email {} registers as teacher", requestDto.email());
        return teacherService.register(requestDto);
    }

    @Operation(summary = "Login",
            description = "Login user by their credentials")
    @PostMapping("/login")
    public UserLoginResponseDto login(
            @RequestBody @Valid final UserLoginRequestDto requestDto) {
        log.info("User with email {} logins", requestDto.email());
        return authenticationService.authenticate(requestDto);
    }
}
