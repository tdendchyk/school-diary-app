package tdenchyk.schooldiaryapp.dto.schedule;

import jakarta.validation.constraints.NotNull;
import java.time.LocalTime;
import tdenchyk.schooldiaryapp.model.Schedule;

public record CreateScheduleDto(@NotNull(message = "You cannot leave it empty")
                                Long classId,
                                @NotNull(message = "You cannot leave it empty")
                                Schedule.Day day,
                                @NotNull(message = "You cannot leave it empty")
                                Long subjectId,
                                @NotNull(message = "You cannot leave it empty")
                                LocalTime startTime,
                                @NotNull(message = "You cannot leave it empty")
                                LocalTime endTime) {
}
