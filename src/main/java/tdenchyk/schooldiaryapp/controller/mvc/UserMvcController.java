package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tdenchyk.schooldiaryapp.dto.user.UserUpdateRequestDto;
import tdenchyk.schooldiaryapp.exception.EntityNotFoundException;
import tdenchyk.schooldiaryapp.model.User;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.ClassService;
import tdenchyk.schooldiaryapp.service.UserService;

@Controller
@RequestMapping("/users")
@Tag(name = "MVC User Controller",
        description = "Views to browse teachers and students")
@RequiredArgsConstructor
public class UserMvcController {
    private final UserService userService;
    private final ClassService classService;
    private final UserRepository userRepository;

    @Operation(summary = "Get a student by id")
    @GetMapping("/students/{id}")
    public String getStudentById(@PathVariable final Long id,
                                 Model model) {
        model.addAttribute("student", userService.findStudentById(id));
        return "user/student-info";
    }

    @Operation(summary = "Get a teacher by id")
    @GetMapping("/teachers/{id}")
    public String getTeacherById(@PathVariable final Long id,
                                 Model model) {
        model.addAttribute("teacher", userService.findTeacherById(id));
        return "user/teacher-info";
    }

    @Operation(summary = "Get all students")
    @GetMapping("/students/all")
    public String getAllStudents(Model model) {
        model.addAttribute("students", userService.findAllStudents());
        return "user/all-students";
    }

    @Operation(summary = "Get all teachers")
    @GetMapping("/teachers/all")
    public String getAllTeachers(Model model) {
        model.addAttribute("teachers", userService.findAllTeachers());
        return "user/all-teachers";
    }

    @Operation(summary = "Get all students by class id")
    @GetMapping("/classes/{classId}/students")
    public String getAllStudentsByClassId(@PathVariable("classId") final Long classId,
                                          Model model) {
        model.addAttribute("students", userService.findAllStudentsByClassId(classId));
        model.addAttribute("class", classService.findClassById(classId));
        return "user/students-by-classid";
    }

    @Operation(summary = "Get all teachers by class id")
    @GetMapping("/classes/{classId}/teachers")
    public String getAllTeachersByClassId(@PathVariable("classId") final Long classId,
                                          Model model) {
        model.addAttribute("teachers", userService.findAllTeachersByClassId(classId));
        model.addAttribute("class", classService.findClassById(classId));
        return "user/teachers-by-classid";
    }

    @Operation(summary = "Delete a student",
            description = "Delete a student by id")
    @PostMapping("/student/delete/{id}")
    public String deleteStudent(@PathVariable final Long id) {
        userService.deleteStudentById(id);
        return "user/student-successfully-deleted";
    }

    @Operation(summary = "Delete a teacher",
            description = "Delete a teacher by id")
    @PostMapping("/teacher/delete/{id}")
    public String deleteTeacher(@PathVariable final Long id) {
        userService.deleteTeacherById(id);
        return "user/teacher-successfully-deleted";
    }

    @GetMapping("/about-me")
    public String showDetailedInfoOnUser(final Authentication authentication,
                                         Model model) {
        User user = (User) authentication.getPrincipal();
        model.addAttribute("user", user);
        return "user/about-me";
    }

    @GetMapping("/update/{id}")
    public String showFormToUpdateInfoOnUser(
            @PathVariable Long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("User doesn't exist.")
        );
        model.addAttribute("user", user);
        return "user/update";
    }

    @Operation(summary = "Update info on user",
            description = "Update firstName, lastName, email, password or phoneNumber")
    @PutMapping("/update/{id}")
    public String updateInfoOnUser(
            @ModelAttribute("user") @Valid UserUpdateRequestDto updateRequestDto,
            Model model, BindingResult result, @PathVariable Long id) {
        if (result.hasErrors()) {
            model.addAttribute("message", "Something went wrong");
            return "user/update";
        }
        User user = userRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("User doesn't exist.")
        );
        try {
            userService.updateUser(user, updateRequestDto);
            return "redirect:/users/about-me";
        } catch (RuntimeException ex) {
            model.addAttribute("message", ex.getMessage());
            model.addAttribute("errors", result.getAllErrors());
            return "user/update";
        }
    }
}
