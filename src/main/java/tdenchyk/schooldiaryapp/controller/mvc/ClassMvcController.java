package tdenchyk.schooldiaryapp.controller.mvc;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.ClassSubjectsUpdateDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;
import tdenchyk.schooldiaryapp.dto.subject.SubjectResponseDto;
import tdenchyk.schooldiaryapp.model.Subject;
import tdenchyk.schooldiaryapp.service.ClassService;
import tdenchyk.schooldiaryapp.service.SubjectService;

@RequiredArgsConstructor
@Controller
@Tag(name = "MVC Class Controller",
        description = "Views to retrieve, update or add classes")
@RequestMapping("/class")
public class ClassMvcController {
    private final ClassService classService;
    private final SubjectService subjectService;

    @Operation(summary = "Retrieve all classes with subjects' ids from DB")
    @Transactional
    @GetMapping("/all")
    public String getAllClasses(Model model) {
        List<ClassResponseDto> allClasses = classService.findAllClasses();
        model.addAttribute("classes", allClasses);
        List<List<String>> subjectNamesOfAllClasses = new ArrayList<>();
        for (ClassResponseDto oneClass : allClasses) {
            List<String> subjectsOfOneClass = oneClass.subjectsId().stream()
                    .map(subjectService::findSubjectById)
                    .map(Subject::getSubjectName)
                    .toList();
            subjectNamesOfAllClasses.add(subjectsOfOneClass);
        }
        model.addAttribute("subjects", subjectNamesOfAllClasses);
        return "class/classes-list";
    }

    @Operation(summary = "Form to add new class")
    @GetMapping("/add-class")
    public String showFormToAddNewClass(
            @ModelAttribute("newClass") CreateClassRequestDto requestDto,
            Model model) {
        model.addAttribute("subjects", subjectService.findAllSubjects());
        return "class/add-class-form";
    }

    @Operation(summary = "Add new class to DB")
    @PostMapping("/add-class")
    public String addNewClass(
            @ModelAttribute("newClass") @Valid final CreateClassRequestDto requestDto,
            BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("subjects", subjectService.findAllSubjects());
            return "class/add-class-form";
        }

        try {
            classService.addNewClass(requestDto);
            return "redirect:/class/all";
        } catch (RuntimeException ex) {
            model.addAttribute("errorMessage", "Such class already exists");
            List<SubjectResponseDto> subjects = subjectService.findAllSubjects();
            model.addAttribute("subjects", subjects);
            return "class/add-class-form";
        }
    }

    @Operation(summary = "Show form to update subjects for a certain class")
    @GetMapping("/{id}/update")
    public String showFormToUpdateSubjects(
            @ModelAttribute("updatedClass") ClassSubjectsUpdateDto updateDto,
            @PathVariable("id") Long id,
            Model model) {
        model.addAttribute("classNumber", classService.findClassById(id).getNumber());
        model.addAttribute("subjects", subjectService.findAllSubjects());
        return "class/update-class-subjects";
    }

    @Operation(summary = "Update subjects for a certain class")
    @PostMapping("/{id}/update")
    public String updateSubjects(
            @ModelAttribute("updatedClass") @Valid final ClassSubjectsUpdateDto updateDto,
            @PathVariable("id") final Long id,
            BindingResult result,
            Model model) {
        if (result.hasErrors()) {
            model.addAttribute("classNumber", classService.findClassById(id).getNumber());
            model.addAttribute("subjects", subjectService.findAllSubjects());
            return "class/update-class-subjects";
        }

        try {
            classService.updateClassSubjects(id, updateDto);
            return "redirect:/class/all";
        } catch (RuntimeException ex) {
            model.addAttribute("errorMessage", "Try again.");
            return "class/update-class-subjects";
        }
    }
}
