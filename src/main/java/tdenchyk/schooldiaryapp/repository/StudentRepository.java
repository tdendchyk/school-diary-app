package tdenchyk.schooldiaryapp.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tdenchyk.schooldiaryapp.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query("select distinct s from Student s where s.classNumberId.id =:classId")
    List<Student> findAllByClassId(@Param("classId") final Long classId);

    Optional<Student> findByEmail(String email);

}
