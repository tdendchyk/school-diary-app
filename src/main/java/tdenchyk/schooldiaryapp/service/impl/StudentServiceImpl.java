package tdenchyk.schooldiaryapp.service.impl;

import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterResponseDto;
import tdenchyk.schooldiaryapp.exception.RegistrationException;
import tdenchyk.schooldiaryapp.mapper.StudentMapper;
import tdenchyk.schooldiaryapp.model.Role;
import tdenchyk.schooldiaryapp.model.Student;
import tdenchyk.schooldiaryapp.repository.RoleRepository;
import tdenchyk.schooldiaryapp.repository.UserRepository;
import tdenchyk.schooldiaryapp.service.StudentService;

@Service
@Log4j2
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final StudentMapper studentMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public StudentRegisterResponseDto register(final StudentRegisterRequestDto requestDto) {
        if (userRepository.existsByEmail(requestDto.email())) {
            log.error("Failed to register user with email {}, it already exists",
                    requestDto.email());
            throw new RegistrationException("Unable to complete registration. User already exists");
        }
        Role defaultRole = roleRepository.findRoleByName(Role.RoleName.STUDENT);
        Student student = studentMapper.toStudent(requestDto);
        student.setRoles(Set.of(defaultRole));
        student.setPassword(passwordEncoder.encode(requestDto.password()));
        return studentMapper.toDto(userRepository.save(student));
    }
}
