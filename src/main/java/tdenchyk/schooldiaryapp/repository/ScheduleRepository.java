package tdenchyk.schooldiaryapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tdenchyk.schooldiaryapp.model.Schedule;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    @Query("""
            select distinct sc from Schedule sc
            left join fetch sc.schoolClass c
            where c.id =:classId and sc.day =:day""")
    List<Schedule> findSchedulesByDayAndClassId(
            @Param("classId") Long classId,
            @Param("day") Schedule.Day day);

    List<Schedule> findSchedulesBySubjectId(Long subjectId);
}
