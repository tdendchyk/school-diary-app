package tdenchyk.schooldiaryapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tdenchyk.schooldiaryapp.model.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {
    @Query("select distinct s from Class c join c.subjects s where c.id =:classNameId")
    List<Subject> findAllByClassNameId(@Param("classNameId") final Long classNameId);
}
