package tdenchyk.schooldiaryapp.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import tdenchyk.schooldiaryapp.dto.teacher.TeacherRegisterRequestDto;
import tdenchyk.schooldiaryapp.validator.annotation.PasswordMatch;

public class StudentFieldMatchValidator implements ConstraintValidator<PasswordMatch,
        TeacherRegisterRequestDto> {
    @Override
    public boolean isValid(TeacherRegisterRequestDto value, ConstraintValidatorContext context) {
        String firstField = value.password();
        String secondField = value.repeatPassword();
        return firstField != null && firstField.equals(secondField);
    }
}
