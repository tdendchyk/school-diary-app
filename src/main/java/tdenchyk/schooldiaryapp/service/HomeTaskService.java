package tdenchyk.schooldiaryapp.service;

import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.model.User;

public interface HomeTaskService {
    HomeTaskResponseDto addHomeTask(final User user,
                                    final HomeTaskRequestDto requestDto);

    HomeTaskResponseDto addHomeTask(final HomeTaskRequestDto requestDto);

    HomeTaskResponseDto updateHomeTask(final Long id,
                                       final User user,
                                       final HomeTaskRequestDto requestDto);

    HomeTaskResponseDto findById(final Long id);

    void deleteById(final Long id);
}
