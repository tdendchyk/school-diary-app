package tdenchyk.schooldiaryapp.controller.mvc;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tdenchyk.schooldiaryapp.SchoolDiaryAppApplication;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SchoolDiaryAppApplication.class)
public class ScheduleMvcControllerTest {
    protected static MockMvc mockMvc;

    @BeforeAll
    static void beforeAll(@Autowired WebApplicationContext applicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Test
    @DisplayName("Form to get students' schedule")
    public void showFormToGetStudentsSchedule_Success() throws Exception {
        mockMvc.perform(get("/schedules/students"))
                .andExpect(status().isOk())
                .andExpect(view().name("schedule/student-schedule"))
                .andExpect(model().attributeExists("classes"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Test
    @DisplayName("Form to get teachers' schedule")
    public void showFormToGetTeachersSchedule_Success() throws Exception {
        mockMvc.perform(get("/schedules/teachers"))
                .andExpect(status().isOk())
                .andExpect(view().name("schedule/teacher-schedule"))
                .andExpect(model().attributeExists("subjects"))
                .andExpect(model().attributeExists("classes"));
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Test
    @DisplayName("Form to create schedule")
    public void showFormToCreateStudentsSchedule_Success() throws Exception {
        mockMvc.perform(get("/schedules/students/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("schedule/add-schedule"))
                .andExpect(model().attributeExists("subjects"))
                .andExpect(model().attributeExists("classes"));
    }
}
