package tdenchyk.schooldiaryapp.controller.rest;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.shaded.org.apache.commons.lang3.builder.EqualsBuilder;
import tdenchyk.schooldiaryapp.dto.classes.ClassResponseDto;
import tdenchyk.schooldiaryapp.dto.classes.ClassSubjectsUpdateDto;
import tdenchyk.schooldiaryapp.dto.classes.CreateClassRequestDto;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClassControllerTest {
    protected static MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeAll
    static void beforeAll(@Autowired WebApplicationContext applicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Sql(
            scripts = "classpath:/database/class/add-two-classes.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Get all classes")
    public void getAllClasses_Success() throws Exception {
        List<ClassResponseDto> expected = new ArrayList<>();
        expected.add(new ClassResponseDto(1L, "10", new ArrayList<>()));
        expected.add(new ClassResponseDto(2L, "11", new ArrayList<>()));
        MvcResult result = mockMvc.perform(get("/api/classes")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ClassResponseDto[] actual = objectMapper.readValue(
                result.getResponse().getContentAsByteArray(), ClassResponseDto[].class);
        Assertions.assertEquals(2, actual.length);
        Assertions.assertEquals(expected, Arrays.stream(actual).toList());
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Sql(
            scripts = {"classpath:/database/class/add-two-classes.sql",
                    "classpath:/database/subjects/add-three-default-subjects.sql"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/subjects/delete-all-subjects.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Update class's subjects")
    public void updateSubjects_Success() throws Exception {
        ClassSubjectsUpdateDto request = new ClassSubjectsUpdateDto(List.of(1L, 2L));
        String jsonRequest = objectMapper.writeValueAsString(request);

        ClassResponseDto expected = new ClassResponseDto(1L, "10", List.of(1L, 2L));

        MvcResult result = mockMvc.perform(patch("/api/classes/{id}/update", 1L)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ClassResponseDto actual = objectMapper.readValue(result.getResponse().getContentAsString(), ClassResponseDto.class);
        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.id());
        EqualsBuilder.reflectionEquals(expected, actual);
    }

    @WithMockUser(username = "teacher", roles = {"TEACHER"})
    @Sql(
            scripts = "classpath:/database/subjects/add-three-default-subjects.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    )
    @Sql(
            scripts = {"classpath:/database/class/delete-all-classes.sql",
                    "classpath:/database/subjects/delete-all-subjects.sql"},
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
    )
    @Test
    @DisplayName("Add new class")
    public void addNewClass_validDto_Success() throws Exception {
        CreateClassRequestDto requestDto = new CreateClassRequestDto("9", List.of(1L, 2L));

        ClassResponseDto expected = new ClassResponseDto(1L, "9", List.of(1L, 2L));
        String jsonRequest = objectMapper.writeValueAsString(requestDto);

        MvcResult result = mockMvc.perform(
                        post("/api/classes/add")
                                .content(jsonRequest)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        ClassResponseDto actual = objectMapper.readValue(
                result.getResponse().getContentAsString(), ClassResponseDto.class);
        Assertions.assertNotNull(actual);
        EqualsBuilder.reflectionEquals(expected, actual);
    }
}
