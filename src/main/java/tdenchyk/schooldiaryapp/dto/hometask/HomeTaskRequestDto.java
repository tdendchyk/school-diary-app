package tdenchyk.schooldiaryapp.dto.hometask;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import org.hibernate.validator.constraints.Length;

public record HomeTaskRequestDto(@NotBlank(message = "You cannot leave it empty")
                                 @Length(max = 255, message = "It should contain no more than 255 symbols")
                                 String task,
                                 @Future(message = "It should be a future date")
                                 LocalDateTime taskDeadline,
                                 @NotNull(message = "You should choose something")
                                 Long scheduleId) {
}
