package tdenchyk.schooldiaryapp.dto.classes;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import java.util.List;

public record CreateClassRequestDto(
        @NotBlank(message = "You cannot leave it blank")
        @Pattern(regexp = "^(1[0-2]|[1-9])[A-Za-z]?$",
                message = "It should contain a number first and a letter after if needed")
        String number,
        @NotEmpty(message = "You cannot leave it empty")
        List<Long> subjectsId) {
}
