package tdenchyk.schooldiaryapp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;

@Getter
@Setter
@SQLRestriction(value = "is_deleted=false")
@SQLDelete(sql = "UPDATE classes SET is_deleted = true WHERE id=?")
@Entity
@Table(name = "classes")
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String number;

    @OneToMany(mappedBy = "classNumberId")
    private List<Student> students = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "classes_teachers",
            joinColumns = @JoinColumn(name = "class_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private List<Teacher> teachers = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "classes_subjects",
            joinColumns = @JoinColumn(name = "class_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id"))
    private List<Subject> subjects = new ArrayList<>();

    @Column(nullable = false)
    private boolean isDeleted = false;

    public void addTeacher(Teacher teacher) {
        this.teachers.add(teacher);
        List<Class> classes = teacher.getClasses();
        classes.add(this);
        teacher.setClasses(classes);
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
        List<Class> classes = subject.getClasses();
        classes.add(this);
        subject.setClasses(classes);
    }
}
