package tdenchyk.schooldiaryapp.exception;

public class RegistrationException extends RuntimeException {
    public RegistrationException(final String message) {
        super(message);
    }
}
