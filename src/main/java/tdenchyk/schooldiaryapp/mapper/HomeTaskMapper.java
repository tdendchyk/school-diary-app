package tdenchyk.schooldiaryapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tdenchyk.schooldiaryapp.config.MapperConfig;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskRequestDto;
import tdenchyk.schooldiaryapp.dto.hometask.HomeTaskResponseDto;
import tdenchyk.schooldiaryapp.model.HomeTask;

@Mapper(config = MapperConfig.class)
public interface HomeTaskMapper {
    @Mapping(source = "schedule.id", target = "scheduleId")
    HomeTaskResponseDto toDto(final HomeTask homeTask);

    @Mapping(target = "schedule", ignore = true)
    HomeTask toEntity(final HomeTaskRequestDto requestDto);
}
