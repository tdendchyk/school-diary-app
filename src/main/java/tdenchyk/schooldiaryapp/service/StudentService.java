package tdenchyk.schooldiaryapp.service;

import tdenchyk.schooldiaryapp.dto.student.StudentRegisterRequestDto;
import tdenchyk.schooldiaryapp.dto.student.StudentRegisterResponseDto;

public interface StudentService {
    StudentRegisterResponseDto register(final StudentRegisterRequestDto requestDto);
}
